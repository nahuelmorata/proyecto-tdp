package morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "cuenta",
    indices = {
        @Index(value = "id", unique = true)
    })
public class Cuenta {
    @PrimaryKey
    private int id;

    @NonNull
    private String token;

    public Cuenta(int id, @NonNull String token) {
        this.id = id;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getToken() {
        return token;
    }
}
