package morata.nahuel.organizadoruni.Sistema.SistemaPromocion;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;

public abstract class SistemaPromocion implements Parcelable {
    /*public abstract void agregarNota(int numeroParcial, float nota);*/

    public SistemaPromocion(Parcel in) {
        readFromParcel(in);
    }

    public SistemaPromocion() {

    }

    public abstract boolean promociono();

    public abstract Fragment getFragmento(MateriaCursando materiaCursando);

    public abstract int getCodigo();

    public String obtenerDataGuardar() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("sistemaPromocion", getCodigo());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    private void readFromParcel(Parcel in) {

    }
}
