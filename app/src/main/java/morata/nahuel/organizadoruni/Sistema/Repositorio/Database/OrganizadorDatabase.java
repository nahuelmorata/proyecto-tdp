package morata.nahuel.organizadoruni.Sistema.Repositorio.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CarreraDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CorrelativaDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CuentaCarreraDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CuentaDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.MateriaCursandoDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.MateriaDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Correlativa;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Cuenta;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.CuentaCarrera;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Materia;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.MateriaCursando;

@Database(entities = {Carrera.class, Materia.class, Correlativa.class, CuentaCarrera.class, Cuenta.class, MateriaCursando.class}, version = 1)
public abstract class OrganizadorDatabase extends RoomDatabase {
    public abstract CarreraDAO carreraDAO();
    public abstract MateriaDAO materiaDAO();
    public abstract CorrelativaDAO correlativaDAO();
    public abstract CuentaCarreraDAO cuentaCarreraDAO();
    public abstract CuentaDAO cuentaDAO();
    public abstract MateriaCursandoDAO materiaCursandoDAO();

    private static OrganizadorDatabase INSTANCE;

    public static OrganizadorDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (OrganizadorDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), OrganizadorDatabase.class, "organizador_database")
                            .build();
                }
            }
        }

        return INSTANCE;
    }
}
