package morata.nahuel.organizadoruni.Sistema.SistemaCursado;

import android.os.Parcel;
import android.support.v4.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.LinkedList;

import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.Parcial;
import morata.nahuel.organizadoruni.fragmentos.cursadas.parcialesRecuperatorioGeneral.FragmentoCursadoParcialesRecuperatorioGeneral;

public class CursadoParcialesRecuperatorioGeneral extends SistemaCursado {
    private static final int CODIGO = 0;
    public static final Creator<CursadoParcialesRecuperatorioGeneral> CREATOR = new Creator<CursadoParcialesRecuperatorioGeneral>() {
        @Override
        public CursadoParcialesRecuperatorioGeneral createFromParcel(Parcel source) {
            return new CursadoParcialesRecuperatorioGeneral(source);
        }

        @Override
        public CursadoParcialesRecuperatorioGeneral[] newArray(int size) {
            return new CursadoParcialesRecuperatorioGeneral[size];
        }
    };

    private Parcial recuperatorio;

    public CursadoParcialesRecuperatorioGeneral(Parcel in) {
        super(in);
    }
    public CursadoParcialesRecuperatorioGeneral(int cantidadParciales) {
        super(cantidadParciales);

        this.recuperatorio = new Parcial(0);
    }

    @SuppressWarnings("unused")
    public CursadoParcialesRecuperatorioGeneral(JSONObject dataCursado, Materia materia) {
        try {
            JSONArray arrayParciales = dataCursado.getJSONArray("parciales");

            cantidadParciales = arrayParciales.length();

            this.parciales = new Parcial[cantidadParciales];

            for (int i = 0; i < cantidadParciales; i++) {
                this.parciales[i] = new Parcial(i);
            }

            this.condiciones = new LinkedList<>();

            this.cargarParciales(arrayParciales);
            this.cargarCondiciones(dataCursado.getJSONObject("condiciones"), materia);

            this.recuperatorio = new Parcial(0);

            JSONObject dataRecuperatorio = dataCursado.getJSONObject("recuperatorio");

            recuperatorio.setNota(dataRecuperatorio.getInt("nota"));
            recuperatorio.setFecha(dataRecuperatorio.getString("fecha"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean aprobado() {
        return false;
    }

    @Override
    public int getCodigo() {
        return CODIGO;
    }

    @Override
    public Fragment getFragmento(MateriaCursando materiaCursando) {
        return FragmentoCursadoParcialesRecuperatorioGeneral.newInstance(materiaCursando);
    }

    public Parcial getRecuperatorio() {
        return this.recuperatorio;
    }

    private void cargarParciales(JSONArray dataParciales) throws JSONException {
        for (int i = 0; i < dataParciales.length(); i++) {
            JSONObject parcialObjeto = dataParciales.getJSONObject(i);

            parciales[i].setNota(parcialObjeto.getInt("nota"));
            parciales[i].setFecha(parcialObjeto.getString("fecha"));
        }
    }

    private void cargarCondiciones(JSONObject dataCondiciones, Materia materia) throws JSONException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Iterator<String> it = dataCondiciones.keys();
        while (it.hasNext()) {
            String key = it.next();

            JSONObject dataCondicion = dataCondiciones.getJSONObject(key);

            Condicion condicion = materia.getCondicionesCursado().get(Integer.parseInt(key)).getConstructor(JSONObject.class).newInstance(dataCondicion);

            agregarCondicion(condicion);
        }
    }

    @Override
    public String obtenerDataGuardar() {
        String dataJSON = "";

        try {
            JSONObject dataGuardar = new JSONObject(super.obtenerDataGuardar());

            JSONObject dataRecuperatorio = new JSONObject();

            dataRecuperatorio.put("nota", recuperatorio.getNota());
            dataRecuperatorio.put("fecha", recuperatorio.getFechaGuardar());

            dataGuardar.put("recuperatorio", dataRecuperatorio);

            dataJSON = dataGuardar.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dataJSON;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeParcelable(recuperatorio, flags);
    }

    @Override
    protected void readFromParcel(Parcel in) {
        super.readFromParcel(in);

        this.recuperatorio = in.readParcelable(Parcial.class.getClassLoader());
    }
}
