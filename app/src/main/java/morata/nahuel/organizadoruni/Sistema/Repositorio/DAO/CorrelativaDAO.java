package morata.nahuel.organizadoruni.Sistema.Repositorio.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Correlativa;

@Dao
public interface CorrelativaDAO {
    @Query("SELECT * FROM correlativas WHERE materia_id = :materia_id")
    LiveData<List<Correlativa>> correlativas(int materia_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertar(Correlativa correlativa);

    @Delete
    void borrar(Correlativa correlativa);
}
