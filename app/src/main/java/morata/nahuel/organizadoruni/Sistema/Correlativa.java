package morata.nahuel.organizadoruni.Sistema;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import morata.nahuel.organizadoruni.Sistema.Materia.Materia;

public class Correlativa implements Parcelable {
    public static final int CURSADO = 0;
    public static final int APROBADO = 1;

    public static final Creator<Correlativa> CREATOR = new Creator<Correlativa>() {
        @Override
        public Correlativa createFromParcel(Parcel source) {
            return new Correlativa(source);
        }

        @Override
        public Correlativa[] newArray(int size) {
            return new Correlativa[size];
        }
    };

    protected Materia materiaCorrelativa;
    protected int estado;
    protected int condicion;

    public Correlativa(Parcel in) {
        readFromParcel(in);
    }

    public Correlativa(Materia materia, int estado, int condicion) {
        this.materiaCorrelativa = materia;
        this.estado = estado;
        this.condicion = condicion;
    }

    public boolean pasaCorrelativa() {
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(materiaCorrelativa, flags);
        dest.writeInt(estado);
        dest.writeInt(condicion);
    }

    private void readFromParcel(Parcel in) {
        materiaCorrelativa = in.readParcelable(Materia.class.getClassLoader());
        estado = in.readInt();
        condicion = in.readInt();
    }
}
