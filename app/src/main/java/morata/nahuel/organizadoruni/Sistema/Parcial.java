package morata.nahuel.organizadoruni.Sistema;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class Parcial implements Parcelable {
    protected Calendar fecha;
    protected int nota;
    protected int numeracion;

    public static final Creator<Parcial> CREATOR = new Creator<Parcial>() {
        @Override
        public Parcial createFromParcel(Parcel source) {
            return new Parcial(source);
        }

        @Override
        public Parcial[] newArray(int size) {
            return new Parcial[size];
        }
    };

    public Parcial(Parcel in) {
        this.fecha = Calendar.getInstance();
        readFromParcel(in);
    }

    public Parcial(int numeracion){
        this.nota = 0;
        this.fecha = Calendar.getInstance();
        this.numeracion = numeracion;
    }

    public int getNota() {
        return this.nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public String getFechaGuardar() {
        return this.fecha.get(Calendar.DATE) + "/" + (this.fecha.get(Calendar.MONTH) + 1) + "/" + this.fecha.get(Calendar.YEAR);
    }

    public void setFecha(String fecha) {
        String[] fechaSplit = fecha.split("/");

        this.fecha.set(Integer.parseInt(fechaSplit[2]), Integer.parseInt(fechaSplit[1]) - 1, Integer.parseInt(fechaSplit[0]));
    }

    public int getNumeracion() {
        return numeracion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getFechaGuardar());
        dest.writeInt(nota);
        dest.writeInt(numeracion);
    }

    private void readFromParcel(Parcel in) {
        setFecha(in.readString());
        nota = in.readInt();
        numeracion = in.readInt();
    }
}
