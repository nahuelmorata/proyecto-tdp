package morata.nahuel.organizadoruni.Sistema;

import android.app.Application;
import android.arch.lifecycle.Observer;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import morata.nahuel.organizadoruni.Constantes;
import morata.nahuel.organizadoruni.Sistema.Carrera.Carrera;
import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Cuenta;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Repositorio;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.CursadoParcialesRecuperatorioGeneral;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.SistemaCursado;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.PromocionDirecta;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.SistemaPromocion;
import morata.nahuel.organizadoruni.Sistema.listener.OnCarrerasChange;

public class Sistema implements Parcelable {
    protected Alumno alumno;
    protected SparseArrayParceable<Carrera> carreras;
    protected SparseArrayParceable<Class<? extends SistemaCursado>> sistemasCursado;
    protected SparseArrayParceable<Class<? extends SistemaPromocion>> sistemasPromocion;
    protected List<OnCarrerasChange> listenersCarreras;

    public static final Creator<Sistema> CREATOR = new Creator<Sistema>() {
        @Override
        public Sistema createFromParcel(Parcel source) {
            return new Sistema(source);
        }

        @Override
        public Sistema[] newArray(int size) {
            return new Sistema[size];
        }
    };

    public Sistema(Parcel in) {
        readFromParcel(in);
    }

    public Sistema(final AppCompatActivity activity) {
        this.alumno = null;
        this.carreras = new SparseArrayParceable<>();

        this.listenersCarreras = new LinkedList<>();

        this.cargarDesdeDB(activity);

        this.cargarSistemasCursado();
        this.cargarSistemasPromocion();
    }

    public void iniciarAlumno(final AppCompatActivity activity, int id, String token, boolean nuevo) {
        final Repositorio repositorio = new Repositorio(activity.getApplication());
        Cuenta cuenta = new Cuenta(id,token);

        this.alumno = new Alumno(id, token);

        if (nuevo) {
            repositorio.insertarCuenta(cuenta);
        }

        repositorio.cargarCarreraCuenta(this);

        repositorio.getCarreraCuentaLiveData(alumno).observe(activity, new Observer<List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera>>() {
            @Override
            public void onChanged(@Nullable List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera> carrerasDB) {
                if (carrerasDB != null) {
                    for (morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera carrera : carrerasDB) {
                        alumno.agregarCarrera(carreras.get(carrera.getId()));
                    }
                }

                repositorio.getMateriasCursandoLiveData(alumno).observe(activity, new Observer<List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.MateriaCursando>>() {
                    @Override
                    public void onChanged(@Nullable List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.MateriaCursando> materiaCursandos) {
                        if (materiaCursandos != null) {
                            for (morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.MateriaCursando materiaCursando : materiaCursandos) {
                                boolean encontre = false;
                                Materia materiaBuscada = null;

                                for (int i = 0; i < alumno.getCarreras().size() && !encontre; i++) {
                                    materiaBuscada = alumno.getCarreras().valueAt(i).getMaterias().get(materiaCursando.getId_materia());
                                    if (materiaBuscada != null) {
                                        encontre = true;
                                    }
                                }

                                if (materiaBuscada != null) {
                                    try {
                                        // Carga sistema cursado
                                        JSONObject jsonObjectCursado = new JSONObject(materiaCursando.getDataCursado());

                                        int idSistemaCursado = (int) jsonObjectCursado.get("sistemaCursado");

                                        Constructor<? extends SistemaCursado> sistemaCursadoConstructor = sistemasCursado.get(idSistemaCursado).getConstructor(JSONObject.class, Materia.class);
                                        SistemaCursado sistemaCursado = sistemaCursadoConstructor.newInstance(jsonObjectCursado, materiaBuscada);

                                        // Carga sistema promocion
                                        JSONObject jsonObjectPromocion = new JSONObject(materiaCursando.getDataPromocion());

                                        SistemaPromocion sistemaPromocion = null;

                                        if (!jsonObjectPromocion.isNull("sistemaPromocion")) {
                                            int idSistemaPromocion = (int) jsonObjectPromocion.get("sistemaPromocion");

                                            Constructor<? extends SistemaPromocion> sistemaPromocionConstructor = sistemasPromocion.get(idSistemaPromocion).getConstructor(JSONObject.class);
                                            sistemaPromocion = sistemaPromocionConstructor.newInstance(jsonObjectPromocion);
                                        }

                                        alumno.agregarMateriaCursando(materiaBuscada, sistemaCursado, sistemaPromocion);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    } catch (InstantiationException e) {
                                        e.printStackTrace();
                                    } catch (NoSuchMethodException e) {
                                        e.printStackTrace();
                                    } catch (InvocationTargetException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                });
            }
        });
    }

    public void agregarCarrera(Carrera carrera) {
        carreras.put(carrera.getId(), carrera);

        for (OnCarrerasChange listener : listenersCarreras) {
            listener.onChange();
        }
    }

    /*public void agregarListenerCarrera(OnCarrerasChange onCarrerasChange) {

        this.listenersCarreras.add(onCarrerasChange);
    }*/

    public Alumno getAlumno() {
        return this.alumno;
    }

    public SparseArrayParceable<Carrera> getCarreras() {
        return this.carreras;
    }

    public void agregarCarreraAlumno(Application application, Carrera carrera) {
        Repositorio repositorio = new Repositorio(application);

        repositorio.agregarCarreraCuenta(this.alumno, carrera);

        this.alumno.agregarCarrera(carrera);
    }

    public void borrarCarreraAlumno(Application application, Carrera carrera) {
        Repositorio repositorio = new Repositorio(application);

        repositorio.borrarCarreraCuenta(this.alumno, carrera);

        this.alumno.borrarCarrera(carrera);
    }

    public SistemaCursado obtenerSistemaCursado(String sistemaCursado, int cantidadParciales) {
        switch (sistemaCursado) {
            case "Parciales + Recuperatorio general":
                return new CursadoParcialesRecuperatorioGeneral(cantidadParciales);
            default:
                return null;
        }
    }

    public SistemaPromocion obtenerSistemaPromocion(String sistemaPromocion) {
        switch (sistemaPromocion) {
            case "Promocion Directa":
                return new PromocionDirecta();
            default:
                return null;
        }
    }

    public Class<? extends SistemaCursado> obtenerSistemaCursado2(int id) {
        return this.sistemasCursado.get(id);
    }

    public void agregarMateriaCursandoAlumno(Application application, Materia materia, String sistemaCursado, int cantidadParcialesCursado, ArrayList<Condicion> condiciones, String sistemaPromocion) {
        SistemaCursado sistemaCursadoParseado = obtenerSistemaCursado(sistemaCursado, cantidadParcialesCursado);
        SistemaPromocion sistemaPromocionParseado = obtenerSistemaPromocion(sistemaPromocion);

        for (Condicion condicion : condiciones) {
            sistemaCursadoParseado.agregarCondicion(condicion);
        }

        MateriaCursando materiaCursando = alumno.agregarMateriaCursando(materia, sistemaCursadoParseado, sistemaPromocionParseado);

        if (materiaCursando != null) {
            Repositorio repositorio = new Repositorio(application);
            repositorio.agregarMateriaCursando(materiaCursando, alumno);
        }
    }

    public void cerrarSesion(Application application) {
        new Repositorio(application).cerrarSesion();
    }

    private void cargarSistemasCursado() {
        this.sistemasCursado = new SparseArrayParceable<>();

        for (Object[] datosSistemaCursado : Constantes.SISTEMAS_CURSADO_NUEVO) {
            this.sistemasCursado.put((int) datosSistemaCursado[0], (Class<? extends SistemaCursado>) datosSistemaCursado[1]);
        }
    }

    private void cargarSistemasPromocion() {
        this.sistemasPromocion = new SparseArrayParceable<>();

        for (Object[] datosSistemaPromocion : Constantes.SISTEMAS_PROMOCION_NUEVO) {
            this.sistemasPromocion.put((int) datosSistemaPromocion[0], (Class<? extends SistemaPromocion>) datosSistemaPromocion[1]);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(alumno, flags);
        dest.writeParcelable(carreras, flags);
        dest.writeParcelable(sistemasCursado, flags);
        dest.writeParcelable(sistemasPromocion, flags);
    }

    private void readFromParcel(Parcel in) {
        alumno = in.readParcelable(Alumno.class.getClassLoader());
        carreras = in.readParcelable(SparseArrayParceable.class.getClassLoader());
        sistemasCursado = in.readParcelable(SparseArrayParceable.class.getClassLoader());
        sistemasPromocion = in.readParcelable(SparseArrayParceable.class.getClassLoader());
    }

    private void cargarDesdeDB(final AppCompatActivity activity) {
        final Repositorio repositorio = new Repositorio(activity.getApplication());

        repositorio.getLiveDataCarreras().observe(activity, new Observer<List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera>>() {
            @Override
            public void onChanged(@Nullable List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera> listaCarreras) {
                if (listaCarreras != null) {
                    for (final morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera carrera : listaCarreras) {
                        final Carrera carreraNueva = new Carrera(carrera.getId(), carrera.getNombre());

                        agregarCarrera(carreraNueva);

                        repositorio.getLiveDataMaterias(carreraNueva.getId()).observe(activity, new Observer<List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Materia>>() {
                            @SuppressWarnings("ConstantConditions")
                            @Override
                            public void onChanged(@Nullable List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Materia> materias) {
                                final SparseArrayParceable<Materia> materiasNuevas = carreraNueva.getMaterias();

                                for (morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Materia materia : materias) {
                                    final Materia materiaNueva = new Materia(materia.getId(), materia.getNombre());

                                    materiasNuevas.put(materiaNueva.getId(), materiaNueva);

                                    repositorio.getLiveDataCorrelativas(materiaNueva.getId()).observe(activity, new Observer<List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Correlativa>>() {
                                        @Override
                                        public void onChanged(@Nullable List<morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Correlativa> correlativas) {
                                            for (morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Correlativa correlativa : correlativas) {
                                                carreraNueva.agregarCorrelativa(materiaNueva, correlativa.getEstado(), correlativa.getCondicion());
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }


                }
            }
        });
    }
}
