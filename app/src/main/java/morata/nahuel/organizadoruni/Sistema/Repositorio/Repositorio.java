package morata.nahuel.organizadoruni.Sistema.Repositorio;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;

import morata.nahuel.organizadoruni.Constantes;
import morata.nahuel.organizadoruni.Sistema.Alumno;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CarreraDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CorrelativaDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CuentaCarreraDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.CuentaDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.MateriaCursandoDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.DAO.MateriaDAO;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Database.OrganizadorDatabase;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Correlativa;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Cuenta;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.CuentaCarrera;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Materia;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.Sistema;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.SistemaCursado;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.SistemaPromocion;
import morata.nahuel.organizadoruni.Sistema.SparseArrayParceable;
import morata.nahuel.organizadoruni.materiasActivity.MostrarMateriasActivity;
import organizadorUni.AgregarCarreraCuentaMutation;
import organizadorUni.AgregarMateriaCursandoMutation;
import organizadorUni.BorrarCarreraCuentaMutation;
import organizadorUni.CarreraQuery;
import organizadorUni.CerrarSesionMutation;
import organizadorUni.ModificarDatosCursandoMutation;

public class Repositorio {
    private CarreraDAO carreraDAO;
    private MateriaDAO materiaDAO;
    private CorrelativaDAO correlativaDAO;
    private CuentaDAO cuentaDAO;
    private CuentaCarreraDAO cuentaCarreraDAO;
    private MateriaCursandoDAO materiaCursandoDAO;

    private LiveData<List<Carrera>> liveDataCarreras;

    public Repositorio(Application application) {
        OrganizadorDatabase db = OrganizadorDatabase.getDatabase(application);

        this.carreraDAO = db.carreraDAO();
        this.materiaDAO = db.materiaDAO();
        this.correlativaDAO = db.correlativaDAO();
        this.cuentaDAO = db.cuentaDAO();
        this.cuentaCarreraDAO = db.cuentaCarreraDAO();
        this.materiaCursandoDAO = db.materiaCursandoDAO();

        this.liveDataCarreras = this.carreraDAO.carreras();

        new ActualizarAsyncTask(carreraDAO, materiaDAO, correlativaDAO).execute(application);
    }

    public LiveData<List<Carrera>> getLiveDataCarreras() {
        return liveDataCarreras;
    }

    public LiveData<List<Materia>> getLiveDataMaterias(int carrera_id) {
        return this.materiaDAO.materias(carrera_id);
    }

    public LiveData<List<Correlativa>> getLiveDataCorrelativas(int materia_id) {
        return this.correlativaDAO.correlativas(materia_id);
    }

    public LiveData<List<Carrera>> getCarreraCuentaLiveData(Alumno alumno) {
        return this.cuentaCarreraDAO.cuentasCarrerasLivedata(alumno.getId());
    }

    public LiveData<List<MateriaCursando>> getMateriasCursandoLiveData(Alumno alumno) {
        return materiaCursandoDAO.obtenerMateriasCursandoLiveData(alumno.getId());
    }

    public void existeUsuario(AppCompatActivity activity) {
        new ExisteUsuarioAsyncTask(cuentaDAO).execute(activity);
    }

    public void cargarCarreraCuenta(Sistema sistema) {
        new CargarCarrerasCuentaAsyncTask(sistema, cuentaCarreraDAO, materiaCursandoDAO).execute();
    }

    public void agregarCarreraCuenta(Alumno alumno, morata.nahuel.organizadoruni.Sistema.Carrera.Carrera carrera) {
        CuentaCarrera cuentaCarrera = new CuentaCarrera(alumno.getId(), carrera.getId());

        new AgregarCarreraCuentaAsyncTask(cuentaCarreraDAO, alumno.getToken()).execute(cuentaCarrera);
    }

    public void borrarCarreraCuenta(Alumno alumno, morata.nahuel.organizadoruni.Sistema.Carrera.Carrera carrera) {
        new BorrarCarreraCuentaAsyncTask(carrera.getId(), cuentaCarreraDAO).execute(alumno);
    }

    public void insertarCuenta(Cuenta cuenta) {
        new InsertarCuentaAsyncTask(this.cuentaDAO).execute(cuenta);
    }

    /*
    public void insertarCarrera(Carrera carrera) {
        new InsertarCarreraAsyncTask(carreraDAO).execute(carrera);
    }

    public void borrarCarrera(Carrera carrera) {
        new BorrarCarreraAsyncTask(carreraDAO).execute(carrera);
    }*/

    public void guardarCursado(morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando materiaCursando) {
        new GuardarCursadoAsyncTask(cuentaDAO, materiaCursandoDAO).execute(materiaCursando);
    }

    public void agregarMateriaCursando(morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando materiaCursando, Alumno alumno) {
        MateriaCursando materiaCursandoDB = new MateriaCursando(alumno.getId(), materiaCursando.getId());

        materiaCursandoDB.setDataCursado(materiaCursando.getSistemaCursado().obtenerDataGuardar());
        if (materiaCursando.getSistemaPromocion() != null) {
            materiaCursandoDB.setDataPromocion(materiaCursando.getSistemaPromocion().obtenerDataGuardar());
        }

        new AgregarMateriaCursandoAsyncTask(materiaCursandoDAO, alumno).execute(materiaCursandoDB);
    }

    public void borrarMateriaCursando(morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando materiaCursando, Alumno alumno) {
        new BorrarMateriaCursandoAsyncTask(materiaCursandoDAO, alumno.getId()).execute(materiaCursando);
    }

    public void cerrarSesion() {
        new CerrarSesionAsyncTask(cuentaDAO, cuentaCarreraDAO, materiaCursandoDAO).execute();
    }

    /*private static class InsertarCarreraAsyncTask extends AsyncTask<Carrera, Void, Void> {
        private CarreraDAO carreraDAO;

        public InsertarCarreraAsyncTask(CarreraDAO carreraDAO) {
            this.carreraDAO = carreraDAO;
        }

        @Override
        protected Void doInBackground(Carrera... carreras) {
            this.carreraDAO.insertar(carreras[0]);

            return null;
        }
    }

    private static class BorrarCarreraAsyncTask extends AsyncTask<Carrera, Void, Void> {
        private CarreraDAO carreraDAO;

        public BorrarCarreraAsyncTask(CarreraDAO carreraDAO) {
            this.carreraDAO = carreraDAO;
        }

        @Override
        protected Void doInBackground(Carrera... carreras) {
            this.carreraDAO.borrar(carreras[0]);

            return null;
        }
    }*/

    private static class InsertarCuentaAsyncTask extends AsyncTask<Cuenta, Void, Void> {
        private CuentaDAO asyncCuentaDAO;

        public InsertarCuentaAsyncTask(CuentaDAO cuentaDAO) {
            this.asyncCuentaDAO = cuentaDAO;
        }

        @Override
        protected Void doInBackground(Cuenta... cuentas) {
            this.asyncCuentaDAO.insert(cuentas[0]);

            return null;
        }
    }

    private static class CargarCarrerasCuentaAsyncTask extends AsyncTask<Void, Void, Void> {
        private Sistema sistema;
        private CuentaCarreraDAO cuentaCarreraDAO;
        private MateriaCursandoDAO materiaCursandoDAO;

        public CargarCarrerasCuentaAsyncTask(Sistema sistema, CuentaCarreraDAO cuentaCarreraDAO, MateriaCursandoDAO materiaCursandoDAO) {
            this.sistema = sistema;
            this.cuentaCarreraDAO = cuentaCarreraDAO;
            this.materiaCursandoDAO = materiaCursandoDAO;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<Carrera> carreras = cuentaCarreraDAO.cuentasCarreras(sistema.getAlumno().getId());

            if (carreras != null) {
                for (Carrera carrera : carreras) {
                    if (sistema.getCarreras().get(carrera.getId()) != null) {
                        sistema.getAlumno().agregarCarrera(sistema.getCarreras().get(carrera.getId()));
                    }
                }

                new CargarMateriasCursandoAsyncTask(sistema, materiaCursandoDAO).execute();
            }

            return null;
        }
    }

    private static class ActualizarAsyncTask extends AsyncTask<Application, Void, Void> {
        private CarreraDAO carreraDAO;
        private MateriaDAO materiaDAO;
        private CorrelativaDAO correlativaDAO;

        public ActualizarAsyncTask(CarreraDAO carreraDAO, MateriaDAO materiaDAO, CorrelativaDAO correlativaDAO) {
            this.carreraDAO = carreraDAO;
            this.materiaDAO = materiaDAO;
            this.correlativaDAO = correlativaDAO;
        }

        @Override
        protected Void doInBackground(final Application... applications) {
            ApolloClient.builder().serverUrl(Constantes.URL).build().query(
                    CarreraQuery.builder().build()
            ).enqueue(new ApolloCall.Callback<CarreraQuery.Data>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NotNull Response<CarreraQuery.Data> response) {
                    List<CarreraQuery.Carrera> carreraList = response.data().carrera();

                    if (carreraList != null) {
                        for (CarreraQuery.Carrera carrera : carreraList) {
                            Carrera carreraNueva = new Carrera(carrera.id(), carrera.nombre());

                            carreraDAO.insertar(carreraNueva);

                            for (CarreraQuery.Materia materia : carrera.materias()) {
                                Materia materiaNueva = new Materia(materia.id(), materia.nombre(), carreraNueva.getId());

                                materiaDAO.insertar(materiaNueva);

                                for (CarreraQuery.Correlativa correlativa : materia.correlativas()) {
                                    Correlativa correlativaNueva = new Correlativa(correlativa.id(), materiaNueva.getId(), correlativa.materia().id(), correlativa.estado(), correlativa.condicion());

                                    correlativaDAO.insertar(correlativaNueva);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull ApolloException e) {

                }
            });

            return null;
        }
    }

    private static class ExisteUsuarioAsyncTask extends AsyncTask<AppCompatActivity, Void, Void> {
        private CuentaDAO cuentaDAO;

        public ExisteUsuarioAsyncTask(CuentaDAO cuentaDAO) {
            this.cuentaDAO = cuentaDAO;
        }

        @Override
        protected Void doInBackground(AppCompatActivity... appCompatActivities) {
            Cuenta cuenta = cuentaDAO.getCuenta();

            if (cuenta != null) {
                Intent intent = new Intent(appCompatActivities[0], MostrarMateriasActivity.class);

                intent.putExtra("ID", cuenta.getId());
                intent.putExtra("TOKEN", cuenta.getToken());
                intent.putExtra("NUEVO", false);

                appCompatActivities[0].startActivity(intent);
            }

            return null;
        }
    }

    private static class CerrarSesionAsyncTask extends AsyncTask<Void, Void, Void> {
        private CuentaDAO cuentaDAO;
        private CuentaCarreraDAO cuentaCarreraDAO;
        private MateriaCursandoDAO materiaCursandoDAO;

        public CerrarSesionAsyncTask(CuentaDAO cuentaDAO, CuentaCarreraDAO cuentaCarreraDAO, MateriaCursandoDAO materiaCursandoDAO) {
            this.cuentaDAO = cuentaDAO;
            this.cuentaCarreraDAO = cuentaCarreraDAO;
            this.materiaCursandoDAO = materiaCursandoDAO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String token = cuentaDAO.getCuenta().getToken();

            ApolloClient.builder().serverUrl(Constantes.URL).build().mutate(
                    CerrarSesionMutation.builder().token(token).build()
            ).enqueue(new ApolloCall.Callback<CerrarSesionMutation.Data>() {
                @Override
                public void onResponse(@NotNull Response<CerrarSesionMutation.Data> response) {

                }

                @Override
                public void onFailure(@NotNull ApolloException e) {

                }
            });

            materiaCursandoDAO.borrarTodo();
            cuentaCarreraDAO.borrarTodo();
            cuentaDAO.borrarTodo();

            return null;
        }
    }

    private static class AgregarCarreraCuentaAsyncTask extends AsyncTask<CuentaCarrera, Void, Void> {
        private CuentaCarreraDAO cuentaCarreraDAO;
        private String token;

        public AgregarCarreraCuentaAsyncTask(CuentaCarreraDAO cuentaCarreraDAO, String token) {
            this.cuentaCarreraDAO = cuentaCarreraDAO;
            this.token = token;
        }

        @Override
        protected Void doInBackground(final CuentaCarrera... cuentaCarreras) {
            boolean tieneCarrera = false;
            List<Carrera> carreras = cuentaCarreraDAO.cuentasCarreras(cuentaCarreras[0].getId_cuenta());
            Iterator<Carrera> it = carreras.iterator();

            while(it.hasNext() && !tieneCarrera) {
                Carrera carrera = it.next();

                if (carrera.getId() == cuentaCarreras[0].getId_carrera()) {
                    tieneCarrera = true;
                }
            }

            if (!tieneCarrera) {
                ApolloClient.builder().serverUrl(Constantes.URL).build().mutate(
                        AgregarCarreraCuentaMutation.builder().token(token).carrera(cuentaCarreras[0].getId_carrera()).build()
                ).enqueue(new ApolloCall.Callback<AgregarCarreraCuentaMutation.Data>() {
                    @Override
                    public void onResponse(@NotNull Response<AgregarCarreraCuentaMutation.Data> response) {
                        cuentaCarreraDAO.insertar(cuentaCarreras[0]);
                    }

                    @Override
                    public void onFailure(@NotNull ApolloException e) {

                    }
                });
            }

            return null;
        }
    }

    private static class BorrarCarreraCuentaAsyncTask extends AsyncTask<Alumno, Void, Void> {
        private int id_carrera;
        private CuentaCarreraDAO cuentaCarreraDAO;

        public BorrarCarreraCuentaAsyncTask(int id_carrera, CuentaCarreraDAO cuentaCarreraDAO) {
            this.id_carrera = id_carrera;
            this.cuentaCarreraDAO = cuentaCarreraDAO;
        }

        @Override
        protected Void doInBackground(final Alumno... alumnos) {
            ApolloClient.builder().serverUrl(Constantes.URL).build().mutate(
                    BorrarCarreraCuentaMutation.builder().token(alumnos[0].getToken()).carrera(id_carrera).build()
            ).enqueue(new ApolloCall.Callback<BorrarCarreraCuentaMutation.Data>() {
                @Override
                public void onResponse(@NotNull Response<BorrarCarreraCuentaMutation.Data> response) {
                    CuentaCarrera cuentaCarrera = cuentaCarreraDAO.getCuenta(alumnos[0].getId(), id_carrera);

                    cuentaCarreraDAO.borrar(cuentaCarrera);
                }

                @Override
                public void onFailure(@NotNull ApolloException e) {

                }
            });
            return null;
        }
    }

    private static class AgregarMateriaCursandoAsyncTask extends AsyncTask<MateriaCursando, Void, Void> {
        private MateriaCursandoDAO materiaCursandoDAO;
        private Alumno alumno;

        public AgregarMateriaCursandoAsyncTask(MateriaCursandoDAO materiaCursandoDAO, Alumno alumno) {
            this.materiaCursandoDAO = materiaCursandoDAO;
            this.alumno = alumno;
        }

        @Override
        protected Void doInBackground(MateriaCursando... materiaCursandos) {
            MateriaCursando materiaCursando = materiaCursandoDAO.obtenerMateriaCursando(materiaCursandos[0].getId_cuenta(), materiaCursandos[0].getId_materia());

            if (materiaCursando == null) {
                this.materiaCursandoDAO.insertar(materiaCursandos[0]);

                ApolloClient.builder().serverUrl(Constantes.URL).build().mutate(
                        AgregarMateriaCursandoMutation.builder().token(alumno.getToken()).materia(materiaCursandos[0].getId_materia()).dataCursado(materiaCursandos[0].getDataCursado()).dataPromocion(materiaCursandos[0].getDataPromocion()).build()
                ).enqueue(new ApolloCall.Callback<AgregarMateriaCursandoMutation.Data>() {
                    @Override
                    public void onResponse(@NotNull Response<AgregarMateriaCursandoMutation.Data> response) {

                    }

                    @Override
                    public void onFailure(@NotNull ApolloException e) {

                    }
                });
            }
            return null;
        }
    }

    private static class CargarMateriasCursandoAsyncTask extends AsyncTask<Void, Void, Void> {
        private Sistema sistema;
        private MateriaCursandoDAO materiaCursandoDAO;

        public CargarMateriasCursandoAsyncTask(Sistema sistema, MateriaCursandoDAO materiaCursandoDAO) {
            this.sistema = sistema;
            this.materiaCursandoDAO = materiaCursandoDAO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<MateriaCursando> materiasCursando = materiaCursandoDAO.obtenerMateriasCursando(sistema.getAlumno().getId());

            SparseArrayParceable<morata.nahuel.organizadoruni.Sistema.Carrera.Carrera> carreras = sistema.getAlumno().getCarreras();

            for (MateriaCursando materiaCursando : materiasCursando) {
                morata.nahuel.organizadoruni.Sistema.Materia.Materia materiaEncontrada = null;

                for (int i = 0; i < carreras.size() && materiaEncontrada == null; i++) {
                    morata.nahuel.organizadoruni.Sistema.Carrera.Carrera carrera = carreras.valueAt(i);

                    materiaEncontrada = carrera.getMaterias().get(materiaCursando.getId_materia());
                }

                if (materiaEncontrada != null) {
                    try {
                        JSONObject jsonObjectCursado = new JSONObject(materiaCursando.getDataCursado());
                        JSONObject jsonObjectPromocion = new JSONObject(materiaCursando.getDataPromocion());

                        int idSistemaCursado = (int) jsonObjectCursado.get("sistemaCursado");
                        int idSistemaPromocion = 0;

                        String parcialesCursadoString = jsonObjectCursado.getString("parciales");
                        JSONArray parcialesCursado = new JSONArray(parcialesCursadoString);
                        int cantidadParcialesCursado = parcialesCursado.length();

                        SistemaCursado sistemaCursado = sistema.obtenerSistemaCursado2(idSistemaCursado).getConstructor().newInstance(cantidadParcialesCursado);
                        SistemaPromocion sistemaPromocion = null;

                        sistema.getAlumno().agregarMateriaCursando(materiaEncontrada, sistemaCursado, sistemaPromocion);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }
    }

    private static class BorrarMateriaCursandoAsyncTask extends AsyncTask<morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando, Void, Void> {
        private MateriaCursandoDAO materiaCursandoDAO;
        private int idAlumno;

        public BorrarMateriaCursandoAsyncTask(MateriaCursandoDAO materiaCursandoDAO, int idAlumno) {
            this.materiaCursandoDAO = materiaCursandoDAO;
            this.idAlumno = idAlumno;
        }

        @Override
        protected Void doInBackground(morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando... materiaCursandos) {
            MateriaCursando materiaCursando = materiaCursandoDAO.obtenerMateriaCursando(idAlumno, materiaCursandos[0].getId());

            materiaCursandoDAO.borrar(materiaCursando);

            return null;
        }
    }

    private static class GuardarCursadoAsyncTask extends AsyncTask<morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando, Void, Void> {
        private CuentaDAO cuentaDAO;
        private MateriaCursandoDAO materiaCursandoDAO;

        public GuardarCursadoAsyncTask(CuentaDAO cuentaDAO, MateriaCursandoDAO materiaCursandoDAO) {
            this.cuentaDAO = cuentaDAO;
            this.materiaCursandoDAO = materiaCursandoDAO;
        }

        @Override
        protected Void doInBackground(morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando... materiaCursandos) {
            Cuenta cuenta = cuentaDAO.getCuenta();

            if (cuenta != null) {
                MateriaCursando materiaCursando = materiaCursandoDAO.obtenerMateriaCursando(cuenta.getId(), materiaCursandos[0].getId());

                materiaCursando.setDataCursado(materiaCursandos[0].getSistemaCursado().obtenerDataGuardar());

                materiaCursandoDAO.actualizar(materiaCursando);

                ApolloClient.builder().serverUrl(Constantes.URL).build().mutate(
                    ModificarDatosCursandoMutation.builder().token(cuenta.getToken()).materia(materiaCursando.getId_materia()).dataCursado(materiaCursandos[0].getSistemaCursado().obtenerDataGuardar()).build()
                ).enqueue(new ApolloCall.Callback<ModificarDatosCursandoMutation.Data>() {
                    @Override
                    public void onResponse(@NotNull Response<ModificarDatosCursandoMutation.Data> response) {

                    }

                    @Override
                    public void onFailure(@NotNull ApolloException e) {

                    }
                });
            }

            return null;
        }
    }
}
