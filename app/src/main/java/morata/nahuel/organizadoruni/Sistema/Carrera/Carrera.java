package morata.nahuel.organizadoruni.Sistema.Carrera;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;
import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Correlativa;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.SistemaCursado;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.SistemaPromocion;
import morata.nahuel.organizadoruni.Sistema.SparseArrayParceable;

public class Carrera implements Parcelable {
    protected int id;
    protected String nombre;
    protected SparseArrayParceable<Materia> materias;
    protected SparseArrayParceable<LinkedList<Correlativa>> correlativas;

    public Carrera(Parcel in) {
        readFromParcel(in);
    }

    public Carrera(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.materias = new SparseArrayParceable<>();
        this.correlativas = new SparseArrayParceable<>();
    }

    public static final Parcelable.Creator<Carrera> CREATOR = new Parcelable.Creator<Carrera>() {

        @Override
        public Carrera createFromParcel(Parcel source) {
            return new Carrera(source);
        }

        @Override
        public Carrera[] newArray(int size) {
            return new Carrera[size];
        }
    };

    public int getId() {
        return this.id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public SparseArrayParceable<Materia> getMaterias() {
        return this.materias;
    }

    public void agregarCorrelativa(Materia materiaCorrelativa, int estado, int condicion) {
        Correlativa correlativa = new Correlativa(materiaCorrelativa, estado, condicion);

        if (this.correlativas.get(materiaCorrelativa.getId()) == null) {
            this.correlativas.put(materiaCorrelativa.getId(), new LinkedList<Correlativa>());
        }

        this.correlativas.get(materiaCorrelativa.getId()).add(correlativa);
    }

    public MateriaCursando cursar(Materia materia, SistemaCursado sistemaCursado, SistemaPromocion sistemaPromocion) {
        if (correlativas.get(materia.getId()) == null) {
            correlativas.put(materia.getId(), new LinkedList<Correlativa>());
        }

        boolean pasaCorrelativas = pasaCorrelativas(correlativas.get(materia.getId()));

        if (pasaCorrelativas) {
            return materia.cursar(sistemaCursado, sistemaPromocion);
        } else {
            return null;
        }
    }

    public boolean pasaCorrelativas(List<Correlativa> correlativas) {
        for (Correlativa correlativa : correlativas) {
            if (!correlativa.pasaCorrelativa()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nombre);
        dest.writeParcelable(materias, flags);
        dest.writeParcelable(correlativas, flags);
    }

    private void readFromParcel(Parcel in) {
        id = in.readInt();
        nombre = in.readString();
        materias = in.readParcelable(SparseArrayParceable.class.getClassLoader());
        correlativas = in.readParcelable(SparseArrayParceable.class.getClassLoader());
    }
}
