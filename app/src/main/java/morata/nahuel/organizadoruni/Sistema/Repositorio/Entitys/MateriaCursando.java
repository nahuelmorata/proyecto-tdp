package morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "materias_cursando",
    foreignKeys = {
        @ForeignKey(entity = Cuenta.class,
            parentColumns = "id",
            childColumns = "id_cuenta"),
        @ForeignKey(entity = Materia.class,
            parentColumns = "id",
            childColumns = "id_materia")
    },
    indices = {
        @Index(value = "id", unique = true),
        @Index("id_cuenta"),
        @Index("id_materia")
    })
public class MateriaCursando {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int id_cuenta;

    private int id_materia;

    private String dataCursado;

    private String dataPromocion;

    public MateriaCursando(int id_cuenta, int id_materia) {
        this.id_cuenta = id_cuenta;
        this.id_materia = id_materia;

        this.dataCursado = "{}";
        this.dataPromocion = "{}";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_cuenta() {
        return id_cuenta;
    }

    public int getId_materia() {
        return id_materia;
    }

    public void setDataCursado(String dataCursado) {
        this.dataCursado = dataCursado;
    }

    public void setDataPromocion(String dataPromocion) {
        this.dataPromocion = dataPromocion;
    }

    public String getDataCursado() {
        return dataCursado;
    }

    public String getDataPromocion() {
        return dataPromocion;
    }
}
