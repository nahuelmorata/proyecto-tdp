package morata.nahuel.organizadoruni.Sistema.Repositorio.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.CuentaCarrera;

@Dao
public interface CuentaCarreraDAO {
    @Query("SELECT carreras.* FROM cuenta_carrera INNER JOIN carreras WHERE cuenta_carrera.id_cuenta = :id_cuenta AND cuenta_carrera.id_carrera = carreras.id")
    LiveData<List<Carrera>> cuentasCarrerasLivedata(int id_cuenta);

    @Query("SELECT carreras.* FROM cuenta_carrera INNER JOIN carreras WHERE cuenta_carrera.id_cuenta = :id_cuenta AND cuenta_carrera.id_carrera = carreras.id")
    List<Carrera> cuentasCarreras(int id_cuenta);

    @Query("SELECT * FROM cuenta_carrera WHERE id_cuenta = :id_cuenta AND id_carrera = :id_carrera")
    CuentaCarrera getCuenta(int id_cuenta, int id_carrera);

    @Insert
    void insertar(CuentaCarrera cuentaCarrera);

    @Delete
    void borrar(CuentaCarrera cuentaCarrera);

    @Query("DELETE FROM cuenta_carrera")
    void borrarTodo();
}
