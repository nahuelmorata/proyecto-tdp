package morata.nahuel.organizadoruni.Sistema;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SparseArrayParceable<E> extends SparseArray<E> implements Parcelable {
    public static final Parcelable.Creator<SparseArrayParceable> CREATOR = new Creator<SparseArrayParceable>() {
        @Override
        public SparseArrayParceable createFromParcel(Parcel source) {
            return new SparseArrayParceable(source);
        }

        @Override
        public SparseArrayParceable[] newArray(int size) {
            return new SparseArrayParceable[size];
        }
    };

    public SparseArrayParceable(int capacity) {
        super(capacity);
    }

    public SparseArrayParceable(Parcel in) {
        super();
        readFromParcel(in);
    }

    public SparseArrayParceable() {
        super();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(size());
        SparseArray<Object> sparseArray = new SparseArray<>(size());

        for (int i = 0; i < size(); i++) {
            sparseArray.put(keyAt(i), valueAt(i));
        }

        dest.writeSparseArray(sparseArray);
    }

    private void readFromParcel(Parcel in) {
        int size = in.readInt();
        SparseArray sparseArray = in.readSparseArray(SparseArrayParceable.class.getClassLoader());

        for (int i = 0; i < size; i++) {
            if (sparseArray != null) {
                put(sparseArray.keyAt(i), (E) sparseArray.valueAt(i));
            }
        }
    }
}
