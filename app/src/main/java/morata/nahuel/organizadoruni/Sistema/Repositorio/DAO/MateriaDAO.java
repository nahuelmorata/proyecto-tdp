package morata.nahuel.organizadoruni.Sistema.Repositorio.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Materia;

@Dao
public interface MateriaDAO {
    @Query("SELECT * FROM materias WHERE carrera_id = :carrera_id")
    LiveData<List<Materia>> materias(int carrera_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertar(Materia materia);

    @Delete
    void borrar(Materia materia);
}
