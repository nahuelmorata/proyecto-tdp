package morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "materias",
        foreignKeys = @ForeignKey(entity = Carrera.class,
                parentColumns = "id",
                childColumns = "carrera_id"),
        indices = {
            @Index(value = "id", unique = true),
            @Index("carrera_id")
        })
public class Materia {
    @PrimaryKey()
    private int id;

    @NonNull
    private String nombre;

    private int carrera_id;

    public Materia(int id, @NonNull String nombre, int carrera_id) {
        this.id = id;
        this.nombre = nombre;
        this.carrera_id = carrera_id;
    }

    @NonNull
    public String getNombre() {
        return nombre;
    }

    public int getId() {
        return id;
    }

    public int getCarrera_id() {
        return carrera_id;
    }
}
