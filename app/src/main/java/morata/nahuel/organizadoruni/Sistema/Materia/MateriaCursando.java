package morata.nahuel.organizadoruni.Sistema.Materia;

import android.app.Application;
import android.os.Parcel;

import morata.nahuel.organizadoruni.Sistema.Repositorio.Repositorio;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.SistemaCursado;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.SistemaPromocion;

public class MateriaCursando extends Materia {
    protected SistemaCursado sistemaCursado;
    protected SistemaPromocion sistemaPromocion;

    public static final Creator<MateriaCursando> CREATOR = new Creator<MateriaCursando>() {
        @Override
        public MateriaCursando createFromParcel(Parcel source) {
            return new MateriaCursando(source);
        }

        @Override
        public MateriaCursando[] newArray(int size) {
            return new MateriaCursando[size];
        }
    };

    public MateriaCursando(Parcel in) {
        super(in);
    }

    public MateriaCursando(int codigo, String nombre, SistemaCursado sistemaCursado, SistemaPromocion sistemaPromocion) {
        super(codigo, nombre);

        this.sistemaCursado = sistemaCursado;
        this.sistemaPromocion = sistemaPromocion;
    }

    public boolean esPromocionable() {
        return this.sistemaPromocion != null;
    }

    public boolean estaCursada() {
        return this.sistemaCursado.aprobado();
    }

    public SistemaCursado getSistemaCursado() {
        return sistemaCursado;
    }

    public SistemaPromocion getSistemaPromocion() {
        return sistemaPromocion;
    }

    public void guardar(Application application) {
        new Repositorio(application).guardarCursado(this);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeParcelable(sistemaCursado, flags);

    }

    protected void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        sistemaCursado = in.readParcelable(SistemaCursado.class.getClassLoader());
    }
}
