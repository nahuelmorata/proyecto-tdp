package morata.nahuel.organizadoruni.Sistema.Repositorio.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.MateriaCursando;

@Dao
public interface MateriaCursandoDAO {
    @Query("SELECT * FROM materias_cursando WHERE id_cuenta = :id_cuenta")
    LiveData<List<MateriaCursando>> obtenerMateriasCursandoLiveData(int id_cuenta);

    @Query("SELECT * FROM materias_cursando WHERE id_cuenta = :id_cuenta")
    List<MateriaCursando> obtenerMateriasCursando(int id_cuenta);

    @Query("SELECT * FROM materias_cursando WHERE id_cuenta = :id_cuenta AND id_materia = :id_materia")
    MateriaCursando obtenerMateriaCursando(int id_cuenta, int id_materia);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertar(MateriaCursando materiaCursando);

    @Delete
    void borrar(MateriaCursando materiaCursando);

    @Update
    void actualizar(MateriaCursando materiaCursando);

    @Query("DELETE FROM materias_cursando")
    void borrarTodo();
}
