package morata.nahuel.organizadoruni.Sistema.Condicion;

import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Parcial;
import morata.nahuel.organizadoruni.fragmentos.condiciones.cursado.CondicionSumatoriaFragmento;

public class CondicionSumatoria extends Condicion {
    public static final int CODIGO = 1;
    public static final String NOMBRE = "Sumatoria";
    public static final int LAYOUT = R.layout.fragment_condicion_sumatoria;

    public static final Creator<CondicionSumatoria> CREATOR = new Creator<CondicionSumatoria>() {
        @Override
        public CondicionSumatoria createFromParcel(Parcel source) {
            return new CondicionSumatoria(source);
        }

        @Override
        public CondicionSumatoria[] newArray(int size) {
            return new CondicionSumatoria[size];
        }
    };

    private int totalSumatoria;

    public CondicionSumatoria(Parcel in) {
        super(in);
    }

    @SuppressWarnings("unused")
    public CondicionSumatoria(View view) {
        EditText txtTotalSumatoria = view.findViewById(R.id.txtTotalSumatoria);

        if (txtTotalSumatoria.getText().toString().isEmpty()) {
            totalSumatoria = 0;
        } else {
            totalSumatoria = Integer.parseInt(txtTotalSumatoria.getText().toString());
        }
    }

    @SuppressWarnings("unused")
    public CondicionSumatoria(JSONObject dataJSON) {
        try {
            this.totalSumatoria = dataJSON.getInt("totalSumatoria");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public CondicionSumatoria(int algo) {

    }

    @Override
    public String getNombre() {
        return NOMBRE;
    }

    @Override
    public int getCodigo() {
        return CODIGO;
    }

    @Override
    public boolean control(Parcial[] parciales, StringBuilder mensaje) {
        int sumatoria = 0;

        for (Parcial parcial : parciales) {
            if (parcial != null) {
                sumatoria += parcial.getNota();
            }
        }

        mensaje.append("La sumatoria total es ");
        mensaje.append(totalSumatoria - sumatoria);
        mensaje.append(".");

        return true;
    }

    @Override
    public JSONObject getDataGuardar() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("totalSumatoria", totalSumatoria);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public int getLayout() {
        return LAYOUT;
    }

    @Override
    public void readFromParcel(Parcel in) {
        totalSumatoria = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(totalSumatoria);
    }
}
