package morata.nahuel.organizadoruni.Sistema;

import android.app.Application;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;
import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Carrera.Carrera;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Repositorio;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.SistemaCursado;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.SistemaPromocion;
import morata.nahuel.organizadoruni.Sistema.listener.OnCarrerasChange;
import morata.nahuel.organizadoruni.Sistema.listener.OnMateriasCursandoChange;

public class Alumno implements Parcelable {
    protected int id;
    protected String token;
    protected SparseArrayParceable<Carrera> carreras;
    protected SparseArrayParceable<MateriaCursando> materiasCursando;

    protected List<OnCarrerasChange> listenerCarreras;
    protected List<OnMateriasCursandoChange> listenerMaterias;

    public static final Creator<Alumno> CREATOR = new Creator<Alumno>() {
        @Override
        public Alumno createFromParcel(Parcel source) {
            return new Alumno(source);
        }

        @Override
        public Alumno[] newArray(int size) {
            return new Alumno[size];
        }
    };

    public Alumno(Parcel in) {
        readFromParcel(in);
    }
    public Alumno(int id, String token) {
        this.id = id;
        this.token = token;

        this.carreras = new SparseArrayParceable<>();
        this.materiasCursando = new SparseArrayParceable<>();

        this.listenerCarreras = new LinkedList<>();
        this.listenerMaterias = new LinkedList<>();
    }

    public int getId() {
        return this.id;
    }

    public String getToken() {
        return this.token;
    }

    public int getCantidadCarreras() {
        return this.carreras.size();
    }

    public SparseArrayParceable<Carrera> getCarreras() {
        return this.carreras;
    }

    public Carrera getCarrera(int carreraSeleccionada) {
        return this.carreras.get(carreraSeleccionada);
    }

    public SparseArrayParceable<MateriaCursando> getMateriasCursando() {
        return this.materiasCursando;
    }

    public void agregarCarrera(Carrera carrera) {
        this.carreras.put(carrera.getId(), carrera);

        notifyCarrera();
    }

    public void agregarListenerCarrera(OnCarrerasChange onCarrerasChange) {
        this.listenerCarreras.add(onCarrerasChange);
    }

    public void borrarCarrera(Carrera carrera) {
        this.carreras.remove(carrera.getId());

        notifyCarrera();
    }

    public MateriaCursando agregarMateriaCursando(Materia materia, SistemaCursado sistemaCursado, SistemaPromocion sistemaPromocion) {
        Carrera carrera = null;

        for (int i = 0; i < carreras.size() && carrera == null; i++) {
            if (carreras.valueAt(i).getMaterias().get(materia.getId()) != null) {
                carrera = carreras.valueAt(i);
            }
        }

        if (carrera != null) {
            MateriaCursando materiaCursando = carrera.cursar(materia, sistemaCursado, sistemaPromocion);

            if (materiaCursando != null) {
                this.materiasCursando.put(materiaCursando.getId(), materiaCursando);

                notifyMateriaCursado();

                return materiaCursando;
            }
        }

        return null;
    }

    public void agregarListenerMateriaCursando(OnMateriasCursandoChange onMateriasCursandoChange) {
        this.listenerMaterias.add(onMateriasCursandoChange);
    }

    public void borrarMateriaCursando(Application application, MateriaCursando materiaCursando) {
        materiasCursando.remove(materiaCursando.getId());

        new Repositorio(application).borrarMateriaCursando(materiaCursando, this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(token);
        dest.writeParcelable(carreras, flags);
        dest.writeParcelable(materiasCursando, flags);
    }

    private void readFromParcel(Parcel in) {
        id = in.readInt();
        token = in.readString();
        carreras = in.readParcelable(SparseArrayParceable.class.getClassLoader());
        materiasCursando = in.readParcelable(SparseArrayParceable.class.getClassLoader());

        listenerMaterias = new LinkedList<>();
        listenerCarreras = new LinkedList<>();
    }

    private void notifyCarrera() {
        for (OnCarrerasChange listener : listenerCarreras) {
            listener.onChange();
        }
    }

    private void notifyMateriaCursado() {
        for (OnMateriasCursandoChange listener : listenerMaterias) {
            listener.onChange();
        }
    }
}
