package morata.nahuel.organizadoruni.Sistema.Materia;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import morata.nahuel.organizadoruni.Constantes;
import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;
import morata.nahuel.organizadoruni.Sistema.Condicion.CondicionParcialMinimaNota;
import morata.nahuel.organizadoruni.Sistema.Condicion.CondicionSumatoria;
import morata.nahuel.organizadoruni.Sistema.Correlativa;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.SistemaCursado;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.SistemaPromocion;
import morata.nahuel.organizadoruni.Sistema.SparseArrayParceable;
import morata.nahuel.organizadoruni.fragmentos.condiciones.cursado.CondicionSumatoriaFragmento;

public class Materia implements Parcelable {
    protected int id;
    protected String nombre;

    protected SparseArrayParceable<Class<? extends Condicion>> condicionesExistenteCursado;
    protected SparseArrayParceable<Class<? extends Condicion>> condicionesExistentePromocion;

    public static final Creator<Materia> CREATOR = new Creator<Materia>() {
        @Override
        public Materia createFromParcel(Parcel source) {
            return new Materia(source);
        }

        @Override
        public Materia[] newArray(int size) {
            return new Materia[size];
        }
    };

    public Materia(Parcel in) {
        readFromParcel(in);

        cargarCondicionesCursado();
        cargarCondicionesPromocion();
    }

    public Materia(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;

        cargarCondicionesCursado();
        cargarCondicionesPromocion();
    }

    protected void cargarCondicionesCursado() {
        condicionesExistenteCursado = new SparseArrayParceable<>();

        for (Object[] datosCondicionCursado : Constantes.CONDICIONES_CURSADO) {
            condicionesExistenteCursado.put((int) datosCondicionCursado[0], (Class<? extends Condicion>) datosCondicionCursado[1]);
        }

        /*condicionesExistenteCursado.put(CondicionParcialMinimaNota.CODIGO, CondicionParcialMinimaNota.class);
        condicionesExistenteCursado.put(CondicionSumatoria.CODIGO, CondicionSumatoria.class);*/
    }

    protected void cargarCondicionesPromocion() {
        condicionesExistentePromocion = new SparseArrayParceable<>();

        for (Object[] datosCondicionPromocion : Constantes.CONDICIONES_PROMOCION) {
            condicionesExistentePromocion.put((int) datosCondicionPromocion[0], (Class<? extends Condicion>) datosCondicionPromocion[1]);
        }
    }

    public SparseArrayParceable<Class<? extends Condicion>> getCondicionesCursado() {
        return this.condicionesExistenteCursado;
    }

    public SparseArrayParceable<Class<? extends Condicion>> getCondicionesExistentePromocion() {
        return this.condicionesExistentePromocion;
    }

    public int getId() {
        return this.id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public MateriaCursando cursar(SistemaCursado sistemaCursado, SistemaPromocion sistemaPromocion) {
        return new MateriaCursando(this.id, this.nombre, sistemaCursado, sistemaPromocion);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nombre);
    }

    protected void readFromParcel(Parcel in) {
        id = in.readInt();
        nombre = in.readString();
    }
}
