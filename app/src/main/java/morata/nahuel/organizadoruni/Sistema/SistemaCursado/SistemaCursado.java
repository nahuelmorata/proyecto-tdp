package morata.nahuel.organizadoruni.Sistema.SistemaCursado;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.Parcial;

public abstract class SistemaCursado implements Parcelable {
    protected int cantidadParciales;
    protected Parcial[] parciales;
    protected List<Condicion> condiciones;

    public SistemaCursado(Parcel in) {
        condiciones = new LinkedList<>();

        readFromParcel(in);
    }
    public SistemaCursado(int cantidadParciales) {
        this.cantidadParciales = cantidadParciales;

        this.parciales = new Parcial[cantidadParciales];

        for (int i = 0; i < this.parciales.length; i++) {
            this.parciales[i] = new Parcial(i);
        }

        this.condiciones = new LinkedList<>();
    }
    public SistemaCursado() {}

    public abstract boolean aprobado();
    public abstract int getCodigo();
    public abstract Fragment getFragmento(MateriaCursando materiaCursando);

    public Parcial[] getParciales() {
        return this.parciales;
    }

    public int getCantidadParciales() {
        return this.parciales.length;
    }

    public boolean terminoParciales() {
        Calendar hoy = Calendar.getInstance();
        boolean rindioTodos = true;

        for (int i = 0; i < parciales.length && rindioTodos; i++) {
            if (!hoy.before(parciales[i].getFecha())) {
                rindioTodos = false;
            }
        }

        return rindioTodos;
    }

    public String obtenerDataGuardar() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArrayParciales = new JSONArray();
        JSONObject jsonObjectParcial;
        JSONObject condicionObjeto;
        JSONObject condicionesJSON = new JSONObject();

        try {
            jsonObject.put("sistemaCursado", getCodigo());

            for (int i = 0;i < cantidadParciales; i++) {
                jsonObjectParcial = new JSONObject();

                jsonObjectParcial.put("nota", parciales[i].getNota());
                jsonObjectParcial.put("fecha", parciales[i].getFechaGuardar());

                jsonArrayParciales.put(i, jsonObjectParcial);
            }

            jsonObject.put("parciales", jsonArrayParciales);

            for (int i = 0; i < condiciones.size(); i++) {
                 condicionObjeto = condiciones.get(i).getDataGuardar();

                 condicionesJSON.put(String.valueOf(condiciones.get(i).getCodigo()), condicionObjeto);
            }
            jsonObject.put("condiciones", condicionesJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    public void agregarCondicion(Condicion condicion) {
        condiciones.add(condicion);
    }

    public String control() {
        StringBuilder mensaje = new StringBuilder();
        Calendar fechaHoy = Calendar.getInstance();
        Parcial[] parcialesControlar = new Parcial[parciales.length];
        int i = 0;

        for (Parcial parcial : parciales) {
            if (parcial.getFecha().before(fechaHoy)) {
                parcialesControlar[i++] = parcial;
            }
        }

        for (Condicion condicion : condiciones) {
            condicion.control(parcialesControlar, mensaje);
        }

        return mensaje.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cantidadParciales);
        dest.writeArray(parciales);

        Condicion[] condicionesArray = new Condicion[condiciones.size()];
        int i = 0;

        for (Condicion condicion : condiciones) {
            condicionesArray[i++] = condicion;
        }

        dest.writeArray(condicionesArray);
    }

    protected void readFromParcel(Parcel in) {
        cantidadParciales = in.readInt();
        Object[] arrayParciales = in.readArray(Parcial.class.getClassLoader());

        parciales = new Parcial[cantidadParciales];

        if (arrayParciales != null) {
            for (int i = 0; i < arrayParciales.length; i++) {
                parciales[i] = (Parcial) arrayParciales[i];
            }
        }

        Object[] arrayCondiciones = in.readArray(Condicion.class.getClassLoader());
        condiciones = new LinkedList<>();

        if (arrayCondiciones != null){
            for (Object arrayCondicion : arrayCondiciones) {
                condiciones.add((Condicion) arrayCondicion);
            }
        }
    }
}
