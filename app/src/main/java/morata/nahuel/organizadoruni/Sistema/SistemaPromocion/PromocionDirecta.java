package morata.nahuel.organizadoruni.Sistema.SistemaPromocion;

import android.os.Parcel;
import android.support.v4.app.Fragment;

import org.json.JSONObject;

import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;

public class PromocionDirecta extends SistemaPromocion {
    private static final int CODIGO = 0;

    public static final Creator<PromocionDirecta> CREATOR = new Creator<PromocionDirecta>() {
        @Override
        public PromocionDirecta createFromParcel(Parcel source) {
            return new PromocionDirecta(source);
        }

        @Override
        public PromocionDirecta[] newArray(int size) {
            return new PromocionDirecta[size];
        }
    };

    public PromocionDirecta(Parcel in) {
        super(in);
    }

    public PromocionDirecta() {
        super();
    }

    public PromocionDirecta(JSONObject datosJSON) {

    }

    @Override
    public boolean promociono() {
        return false;
    }

    @Override
    public Fragment getFragmento(MateriaCursando materiaCursando) {
        return null;
    }

    @Override
    public int getCodigo() {
        return CODIGO;
    }
}
