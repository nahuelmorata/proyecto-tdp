package morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "cuenta_carrera",
    foreignKeys = {
        @ForeignKey(entity = Cuenta.class,
            parentColumns = "id",
            childColumns = "id_cuenta"),
        @ForeignKey(entity = Carrera.class,
            parentColumns = "id",
            childColumns = "id_carrera")
    },
    indices = {
        @Index("id_cuenta"),
        @Index("id_carrera")
    })
public class CuentaCarrera {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int id_cuenta;

    private int id_carrera;

    public CuentaCarrera(int id_cuenta, int id_carrera) {
        this.id_cuenta = id_cuenta;
        this.id_carrera = id_carrera;
    }

    public int getId() {
        return id;
    }

    public int getId_cuenta() {
        return id_cuenta;
    }

    public int getId_carrera() {
        return id_carrera;
    }

    public void setId(int id) {
        this.id = id;
    }
}
