package morata.nahuel.organizadoruni.Sistema.Repositorio.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Carrera;

@Dao
public interface CarreraDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertar(Carrera carrera);

    @Query("SELECT * FROM carreras")
    LiveData<List<Carrera>> carreras();

    @Delete
    void borrar(Carrera carrera);
}
