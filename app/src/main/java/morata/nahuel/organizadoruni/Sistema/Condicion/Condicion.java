package morata.nahuel.organizadoruni.Sistema.Condicion;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import morata.nahuel.organizadoruni.Sistema.Parcial;

public abstract class Condicion implements Parcelable {
    public Condicion(Parcel in) {
        readFromParcel(in);
    }
    public Condicion() {}

    public abstract String getNombre();
    public abstract int getCodigo();
    public abstract boolean control(Parcial[] parciales, StringBuilder mensaje);
    public abstract JSONObject getDataGuardar();
    public abstract int getLayout();

    @Override
    public int describeContents() {
        return 0;
    }

    public abstract void readFromParcel(Parcel in);
}
