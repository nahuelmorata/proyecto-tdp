package morata.nahuel.organizadoruni.Sistema.listener;

import android.os.Parcelable;

public interface OnMateriasCursandoChange {
    void onChange();
}
