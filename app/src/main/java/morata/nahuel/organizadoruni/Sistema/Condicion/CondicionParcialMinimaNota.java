package morata.nahuel.organizadoruni.Sistema.Condicion;

import android.os.Parcel;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Parcial;

public class CondicionParcialMinimaNota extends Condicion {
    public final static int CODIGO = 0;
    public final static String NOMBRE = "Parciales con un minimo de nota";
    public final static int LAYOUT = R.layout.fragment_condicion_minima_nota;

    protected int minimaNota;

    public static final Creator<CondicionParcialMinimaNota> CREATOR = new Creator<CondicionParcialMinimaNota>() {
        @Override
        public CondicionParcialMinimaNota createFromParcel(Parcel source) {
            return new CondicionParcialMinimaNota(source);
        }

        @Override
        public CondicionParcialMinimaNota[] newArray(int size) {
            return new CondicionParcialMinimaNota[size];
        }
    };

    public CondicionParcialMinimaNota(Parcel in) {
        super(in);
    }

    public CondicionParcialMinimaNota(int minimaNota) {
        this.minimaNota = minimaNota;
    }

    @SuppressWarnings("unused")
    public CondicionParcialMinimaNota(View view) {
        EditText txtMinimaNota = view.findViewById(R.id.txtNotaMinima);

        if (txtMinimaNota.getText().toString().isEmpty()) {
            minimaNota = 0;
        } else {
            minimaNota = Integer.parseInt(txtMinimaNota.getText().toString());
        }
    }

    @SuppressWarnings("unused")
    public CondicionParcialMinimaNota(JSONObject dataJSON) {
        try {
            this.minimaNota = dataJSON.getInt("minimaNota");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getNombre() {
        return NOMBRE;
    }

    @Override
    public int getCodigo() {
        return CODIGO;
    }

    @Override
    public boolean control(Parcial[] parciales, StringBuilder mensaje) {
        boolean pasoControl = true;

        if (parciales[0] != null) {
            for (int i = 0; i < parciales.length && pasoControl; i++) {
                if (parciales[i] != null) {
                    if (parciales[i].getNota() < minimaNota) {
                        pasoControl = false;

                        mensaje.append("El parcial ");
                        mensaje.append(parciales[i].getNumeracion() + 1);
                        mensaje.append(" no alcanza con el minimo especificado.");
                    }
                }
            }

            if (pasoControl) {
                mensaje.append("Hasta ahora esta cursando la materia");
            }
        } else {
            mensaje.append("Todavia no rendiste ningun parcial.");
        }

        return pasoControl;
    }

    @Override
    public JSONObject getDataGuardar() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("minimaNota", minimaNota);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    public int getLayout() {
        return LAYOUT;
    }

    @Override
    public void readFromParcel(Parcel in) {
        minimaNota = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(minimaNota);
    }
}
