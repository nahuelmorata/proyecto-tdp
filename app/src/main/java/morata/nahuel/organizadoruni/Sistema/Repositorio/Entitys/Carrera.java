package morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "carreras",
indices = {
        @Index(value = "id", unique = true)
})
public class Carrera {
    @PrimaryKey
    private int id;

    @NonNull
    private String nombre;

    public Carrera(int id, @NonNull String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getNombre() {
        return nombre;
    }
}
