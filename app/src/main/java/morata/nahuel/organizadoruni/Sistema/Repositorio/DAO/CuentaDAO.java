package morata.nahuel.organizadoruni.Sistema.Repositorio.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys.Cuenta;

@Dao
public interface CuentaDAO {
    @Insert
    void insert(Cuenta cuenta);

    @Query("SELECT * FROM cuenta LIMIT 1")
    Cuenta getCuenta();

    @Delete
    void borrarCuenta(Cuenta cuenta);

    @Query("DELETE FROM cuenta")
    void borrarTodo();
}
