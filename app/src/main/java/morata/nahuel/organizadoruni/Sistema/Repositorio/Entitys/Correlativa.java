package morata.nahuel.organizadoruni.Sistema.Repositorio.Entitys;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "correlativas",
        foreignKeys = {
                @ForeignKey(entity = Materia.class,
                        parentColumns = {
                            "id"
                        },
                        childColumns = {
                            "materia_id"
                        }),
                @ForeignKey(entity = Materia.class,
                        parentColumns = {
                            "id"
                        },
                        childColumns = {
                            "materia_correlativa"
                        })
        },
        indices = {
            @Index("materia_id"), @Index("materia_correlativa")
        })
public class Correlativa {
    @PrimaryKey
    private int id;

    private int materia_id;

    private int materia_correlativa;

    private int estado;

    private int condicion;

    public Correlativa(int id, int materia_id, int materia_correlativa, int estado, int condicion) {
        this.id = id;
        this.materia_id = materia_id;
        this.materia_correlativa = materia_correlativa;
        this.estado = estado;
        this.condicion = condicion;
    }

    public int getId() {
        return id;
    }

    public int getMateria_id() {
        return materia_id;
    }

    public int getMateria_correlativa() {
        return materia_correlativa;
    }

    public int getEstado() {
        return estado;
    }

    public int getCondicion() {
        return condicion;
    }
}
