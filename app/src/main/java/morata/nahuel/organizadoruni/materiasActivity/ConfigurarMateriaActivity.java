package morata.nahuel.organizadoruni.materiasActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import morata.nahuel.organizadoruni.Constantes;
import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;
import morata.nahuel.organizadoruni.materiasActivity.configurarCondicionesActivity.ConfigurarCondicionesActivity;

public class ConfigurarMateriaActivity extends AppCompatActivity {
    private static final int CONFIGURARCONDICIONESCURSADO = 1;

    private Spinner comboBoxSistemaCursado;
    private Spinner comboBoxSistemaPromocion;
    private ArrayAdapter<String> adapterSistemaCursado;
    private ArrayAdapter<String> adapterSistemaPromocion;
    private Button btnAgregar;
    private Button btnConfigurarCondicionesCursado;
    private EditText txtCantidadParciales;
    private TextView lblTituloMateria;

    protected Materia materia;
    protected ArrayList<Condicion> condicionesCursado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar_materia);

        Intent intent = getIntent();

        this.materia = intent.getParcelableExtra("MATERIA");

        this.lblTituloMateria = findViewById(R.id.lblNombreMateria);
        this.lblTituloMateria.setText(this.materia.getNombre());

        this.cargarSistemaCursado();
        this.cargarSistemaPromocion();

        this.cargarBotones();
    }

    private void cargarSistemaCursado() {
        this.comboBoxSistemaCursado = findViewById(R.id.comboBoxSistemaCursado);

        this.adapterSistemaCursado = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Constantes.SISTEMAS_CURSADO);
        this.adapterSistemaCursado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.comboBoxSistemaCursado.setAdapter(this.adapterSistemaCursado);

        this.txtCantidadParciales = findViewById(R.id.txtCantidadParciales);
    }

    private void cargarSistemaPromocion() {
        this.comboBoxSistemaPromocion = findViewById(R.id.comboBoxSistemaPromocion);

        this.adapterSistemaPromocion = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Constantes.SISTEMAS_PROMOCION);
        this.adapterSistemaPromocion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        this.comboBoxSistemaPromocion.setAdapter(this.adapterSistemaPromocion);
    }

    private void cargarBotones() {
        this.btnConfigurarCondicionesCursado = findViewById(R.id.btnCondicionesCursado);
        this.btnConfigurarCondicionesCursado.setOnClickListener(new OyenteBotonConfigurar());

        this.btnAgregar = findViewById(R.id.btnAgregar);
        this.btnAgregar.setOnClickListener(new OyenteBotonAgregar());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CONFIGURARCONDICIONESCURSADO) {
                if (data != null) {
                    condicionesCursado = data.getParcelableArrayListExtra("CONDICIONES");
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private class OyenteBotonAgregar implements OnClickListener {

        @Override
        public void onClick(View v) {
            String sistemaCursado = comboBoxSistemaCursado.getSelectedItem().toString();
            int cantidadParcialesCursado = Integer.parseInt(txtCantidadParciales.getText().toString());
            String sistemaPromocion = comboBoxSistemaPromocion.getSelectedItem().toString();

            Intent intent = new Intent();

            intent.putExtra("MATERIA", materia);
            intent.putExtra("SISTEMACURSADO", sistemaCursado);
            intent.putExtra("CANTIDADPARCIALESCURSADO", cantidadParcialesCursado);
            intent.putExtra("CONDICIONESCURSADO", condicionesCursado);
            intent.putExtra("SISTEMAPROMOCION", sistemaPromocion);

            setResult(RESULT_OK, intent);

            finish();
        }
    }

    private class OyenteBotonConfigurar implements OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ConfigurarMateriaActivity.this, ConfigurarCondicionesActivity.class);

            intent.putExtra("CONDICIONESCURSADO", materia.getCondicionesCursado());

            startActivityForResult(intent, CONFIGURARCONDICIONESCURSADO);
        }
    }
}
