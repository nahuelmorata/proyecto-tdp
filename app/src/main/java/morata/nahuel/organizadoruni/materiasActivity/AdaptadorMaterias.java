package morata.nahuel.organizadoruni.materiasActivity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;

public class AdaptadorMaterias extends RecyclerView.Adapter<AdaptadorMaterias.MateriasViewHolder> implements OnClickListener {
    private ArrayList<Materia> materias;
    private OnClickListener listener;

    public AdaptadorMaterias(ArrayList<Materia> materias) {
        this.materias = materias;
    }

    @NonNull
    @Override
    public MateriasViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_materia, viewGroup, false);

        MateriasViewHolder holder = new MateriasViewHolder(itemView);

        itemView.setOnClickListener(this);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MateriasViewHolder materiasViewHolder, int pos) {
        Materia materia = materias.get(pos);

        materiasViewHolder.bindMateria(materia);
    }

    @Override
    public int getItemCount() {
        return materias.size();
    }

    public Materia getMateria(int pos) {
        return materias.get(pos);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void setMaterias(ArrayList<Materia> materias) {
        this.materias = materias;
    }

    @Override
    public void onClick(View v) {
        if (this.listener != null) {
            this.listener.onClick(v);
        }
    }

    public class MateriasViewHolder extends RecyclerView.ViewHolder {
        private TextView labelNombre;

        public MateriasViewHolder(@NonNull View itemView) {
            super(itemView);

            labelNombre = itemView.findViewById(R.id.labelNombreMateria);
        }

        public void bindMateria(Materia materia) {
            labelNombre.setText(materia.getNombre());
        }
    }


}
