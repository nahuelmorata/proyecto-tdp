package morata.nahuel.organizadoruni.materiasActivity.configurarCondicionesActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.util.ArrayList;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;
import morata.nahuel.organizadoruni.Sistema.SparseArrayParceable;

public class ConfigurarCondicionesActivity extends AppCompatActivity {
    private RecyclerView recyclerViewCondicionesCursado;
    private Button btnGuardarCondiciones;

    private SparseArrayParceable<Class<? extends Condicion>> condiciones;
    private AdaptadorCondiciones adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar_condiciones);

        this.recyclerViewCondicionesCursado = findViewById(R.id.recyclerViewCondicionesCursado);
        this.btnGuardarCondiciones = findViewById(R.id.btnGuardarCondiciones);

        Intent intent = getIntent();
        this.condiciones = intent.getParcelableExtra("CONDICIONESCURSADO");

        cargarCondicionesCursado();

        btnGuardarCondiciones.setOnClickListener(new OyenteGuardar());
    }

    private void cargarCondicionesCursado() {
        ArrayList<Class<? extends Condicion>> condicionesArray = sparse2Array();

        adapter = new AdaptadorCondiciones(condicionesArray);

        this.recyclerViewCondicionesCursado.setAdapter(adapter);
        this.recyclerViewCondicionesCursado.setHasFixedSize(true);
        this.recyclerViewCondicionesCursado.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private ArrayList<Class<? extends Condicion>> sparse2Array() {
        ArrayList<Class<? extends Condicion>> condiciones = new ArrayList<>();

        for (int i = 0; i < this.condiciones.size(); i++) {
            condiciones.add(this.condiciones.valueAt(i));
        }

        return condiciones;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);

        finish();
    }

    private class OyenteGuardar implements OnClickListener {

        @Override
        public void onClick(View v) {
             ArrayList<Condicion> condiciones = new ArrayList<>();

             int cantidadViewHolder = recyclerViewCondicionesCursado.getChildCount();

             for (int i = 0; i < cantidadViewHolder; i++) {
                 AdaptadorCondiciones.CondicionesViewHolder viewHolder = (AdaptadorCondiciones.CondicionesViewHolder) recyclerViewCondicionesCursado.getChildViewHolder(recyclerViewCondicionesCursado.getChildAt(i));

                 Condicion condicion = viewHolder.getCondicion();

                 if (condicion != null) {
                     condiciones.add(condicion);
                 }
             }

             Intent intent = new Intent();

             intent.putExtra("CONDICIONES", condiciones);

             setResult(RESULT_OK, intent);

             finish();
        }
    }
}
