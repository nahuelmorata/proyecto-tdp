package morata.nahuel.organizadoruni.materiasActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;

import java.util.ArrayList;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Carrera.Carrera;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;

import static morata.nahuel.organizadoruni.Constantes.CONFIGURAR_MATERIA_REQUEST;

public class AgregarMateriasActivity extends AppCompatActivity {
    protected Carrera carrera;

    private RecyclerView recyclerViewMaterias;
    private AdaptadorMaterias adaptadorMaterias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_materias);

        Intent intent = getIntent();

        this.carrera = intent.getParcelableExtra("CARRERA");

        this.cargarMaterias();
    }

    private void cargarMaterias() {
        this.recyclerViewMaterias = findViewById(R.id.recyclerViewMaterias);

        this.adaptadorMaterias = new AdaptadorMaterias(sparseArray2List(carrera.getMaterias()));

        this.adaptadorMaterias.setOnClickListener(new OyenteMateria());

        this.recyclerViewMaterias.setHasFixedSize(true);
        this.recyclerViewMaterias.setAdapter(this.adaptadorMaterias);
        this.recyclerViewMaterias.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private ArrayList<Materia> sparseArray2List(SparseArray<Materia> materias) {
        ArrayList<Materia> materiasLista = new ArrayList<>();

        for (int i = 0; i < materias.size(); i++) {
            materiasLista.add(materias.valueAt(i));
        }

        return materiasLista;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CONFIGURAR_MATERIA_REQUEST) {
                Intent intent = new Intent();

                intent.putExtra("MATERIA", data.getParcelableExtra("MATERIA"));
                intent.putExtra("SISTEMACURSADO", data.getStringExtra("SISTEMACURSADO"));
                intent.putExtra("CANTIDADPARCIALESCURSADO", data.getIntExtra("CANTIDADPARCIALESCURSADO", 1));
                intent.putExtra("SISTEMAPROMOCION", data.getStringExtra("SISTEMAPROMOCION"));
                intent.putExtra( "CONDICIONESCURSADO", data.getParcelableArrayListExtra("CONDICIONESCURSADO"));

                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    private class OyenteMateria implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int posicion = recyclerViewMaterias.getChildLayoutPosition(v);
            Materia materia = adaptadorMaterias.getMateria(posicion);

            Intent configurar_materia = new Intent(AgregarMateriasActivity.this, ConfigurarMateriaActivity.class);

            configurar_materia.putExtra("MATERIA", materia);

            startActivityForResult(configurar_materia, CONFIGURAR_MATERIA_REQUEST);
        }
    }
}
