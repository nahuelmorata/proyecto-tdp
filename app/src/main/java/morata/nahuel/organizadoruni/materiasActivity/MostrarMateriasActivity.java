package morata.nahuel.organizadoruni.materiasActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import morata.nahuel.organizadoruni.MainActivity.MainActivity;
import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.SparseArrayParceable;
import morata.nahuel.organizadoruni.Sistema.listener.OnCarrerasChange;
import morata.nahuel.organizadoruni.Sistema.listener.OnMateriasCursandoChange;
import morata.nahuel.organizadoruni.actualizador.Actualizador;
import morata.nahuel.organizadoruni.materiaActivity.MostrarMateriaActivity;
import morata.nahuel.organizadoruni.carreraActivity.AgregarCarreraActivity;
import morata.nahuel.organizadoruni.carreraActivity.BorrarCarreraActivity;
import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Alumno;
import morata.nahuel.organizadoruni.Sistema.Carrera.Carrera;
import morata.nahuel.organizadoruni.Sistema.Materia.Materia;
import morata.nahuel.organizadoruni.Sistema.Sistema;

import static morata.nahuel.organizadoruni.Constantes.AGREGAR_CARRERA_REQUEST;
import static morata.nahuel.organizadoruni.Constantes.AGREGAR_MATERIA_REQUEST;
import static morata.nahuel.organizadoruni.Constantes.BORRAR_CARRERA_REQUEST;
import static morata.nahuel.organizadoruni.Constantes.MATERIAS_CURSANDO;
import static morata.nahuel.organizadoruni.Constantes.MATERIAS_TODAS;
import static morata.nahuel.organizadoruni.Constantes.MOSTRAR_MATERIA_REQUEST;

public class MostrarMateriasActivity extends AppCompatActivity {
    private Sistema sistema;

    private Spinner comboBoxCarreras;
    private Spinner comboBoxMostrar;
    private ArrayAdapter<String> adapterCarreras;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private AdaptadorMaterias adaptadorMaterias;

    private SparseArray<Carrera> posicion2Carreras;
    private boolean mostrandoMateriasCursando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materias);

        this.posicion2Carreras = new SparseArray<>();
        Intent intent = getIntent();

        this.iniciarSistema(intent.getIntExtra("ID", 0), intent.getStringExtra("TOKEN"), intent.getBooleanExtra("NUEVO", false));

        mantenerActualizadoNombres();
        mantenerActualizadoNombresMaterias();

        this.toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);

        this.comboBoxMostrar = findViewById(R.id.comboBoxMostrar);
        this.comboBoxMostrar.setOnItemSelectedListener(new OyenteMostrar());
        this.mostrandoMateriasCursando = true;

        Actualizador actualizador = new Actualizador(getApplication());
        actualizador.actualizar(getSupportFragmentManager());

        cargarCarreras();
        cargarMaterias();
    }

    private void iniciarSistema(int id, String token, boolean nuevo) {
        this.sistema = new Sistema(this);
        this.sistema.iniciarAlumno(this, id, token, nuevo);
    }

    private void cargarCarreras() {
        this.comboBoxCarreras = findViewById(R.id.comboBoxCarreras);

        List<String> carreras = new LinkedList<>();
        carreras.add("No tiene ninguna carrera");

        this.adapterCarreras = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, carreras);

        this.adapterCarreras.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.comboBoxCarreras.setAdapter(adapterCarreras);
        this.comboBoxCarreras.setOnItemSelectedListener(new OyenteCarrera());
    }

    private void cargarMaterias() {
        this.recyclerView = findViewById(R.id.materiasRecycler);

        if (this.sistema.getAlumno().getCantidadCarreras() > 0) {
            adaptadorMaterias = new AdaptadorMaterias(this.materiasArrayList(posicion2Carreras.get(0), MATERIAS_CURSANDO));
        } else {
            adaptadorMaterias = new AdaptadorMaterias(new ArrayList<Materia>());
        }

        adaptadorMaterias.setOnClickListener(new OyenteMateria());

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adaptadorMaterias);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void cambioCarrera(Carrera carrera) {
        adaptadorMaterias.setMaterias(materiasArrayList(carrera, MATERIAS_CURSANDO));
        adaptadorMaterias.notifyDataSetChanged();
    }

    @NonNull
    private ArrayList<Materia> materiasArrayList(Carrera carreraSeleccionada, int tipo) {
        Alumno alumno = this.sistema.getAlumno();

        if (tipo == MATERIAS_CURSANDO) {
            return new ArrayList<>(sparseArray2List(alumno.getMateriasCursando()));
        } else {
            Carrera carrera = alumno.getCarrera(carreraSeleccionada.getId());
            return sparseArray2List(carrera.getMaterias());
        }
    }

    private ArrayList<Materia> sparseArray2List(SparseArray<? extends Materia> materias) {
        ArrayList<Materia> materiasLista = new ArrayList<>();

        for (int i = 0; i < materias.size(); i++) {
            materiasLista.add(materias.valueAt(i));
        }

        return materiasLista;
    }

    private void mantenerActualizadoNombres() {
        sistema.getAlumno().agregarListenerCarrera(new OnCarrerasChange() {
            @Override
            public void onChange() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        actualizarCarreras();
                    }
                });
            }
        });
    }

    private void mantenerActualizadoNombresMaterias() {
        sistema.getAlumno().agregarListenerMateriaCursando(new OnMateriasCursandoChange() {
            @Override
            public void onChange() {
                MostrarMateriasActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        actualizarMaterias();
                    }
                });
            }
        });
    }

    private void limpiarCarreras() {
        this.adapterCarreras.clear();
    }

    private void limpiarMaterias() {
        this.adaptadorMaterias.setMaterias(new ArrayList<Materia>());
        this.adaptadorMaterias.notifyDataSetChanged();
    }

    private void limpiarUI() {
        this.limpiarCarreras();

        this.limpiarMaterias();
    }

    private void actualizarCarreras() {
        this.limpiarUI();

        SparseArrayParceable<Carrera> carreras = this.sistema.getAlumno().getCarreras();

        if (carreras.size() > 0) {
            for (int i = 0; i < carreras.size(); i++) {
                this.posicion2Carreras.put(i, carreras.valueAt(i));
                this.adapterCarreras.add(carreras.valueAt(i).getNombre());
            }

            if (this.adapterCarreras.getCount() > 0) {
                cambioCarrera(posicion2Carreras.get(0));
            }
        } else {
            this.adapterCarreras.add("No tiene ninguna carrera");
        }
    }

    private void actualizarMaterias() {
        this.limpiarMaterias();

        Carrera carrera = posicion2Carreras.get(this.comboBoxCarreras.getSelectedItemPosition());

        adaptadorMaterias.setMaterias(materiasArrayList(carrera, MATERIAS_CURSANDO));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == AGREGAR_CARRERA_REQUEST) {
                Carrera carrera = data.getParcelableExtra("CARRERA");

                sistema.agregarCarreraAlumno(getApplication(), carrera);

                actualizarCarreras();
            } else if (requestCode == BORRAR_CARRERA_REQUEST) {
                Carrera carrera = data.getParcelableExtra("CARRERA");

                sistema.borrarCarreraAlumno(getApplication(), carrera);

                actualizarCarreras();
            } else if (requestCode == AGREGAR_MATERIA_REQUEST) {
                Materia materia = data.getParcelableExtra("MATERIA");
                String sistemaCursado = data.getStringExtra("SISTEMACURSADO");
                int cantidadParcialesCursado = data.getIntExtra("CANTIDADPARCIALESCURSADO", 1);
                ArrayList<Condicion> condiciones = data.getParcelableArrayListExtra("CONDICIONESCURSADO");
                String sistemaPromocion = data.getStringExtra("SISTEMAPROMOCION");

                sistema.agregarMateriaCursandoAlumno(getApplication(), materia, sistemaCursado, cantidadParcialesCursado, condiciones, sistemaPromocion);

                actualizarMaterias();
            } else if (requestCode == MOSTRAR_MATERIA_REQUEST) {
                MateriaCursando materiaCursando = data.getParcelableExtra("MATERIACURSADOBORRAR");

                if (materiaCursando != null) {
                    sistema.getAlumno().borrarMateriaCursando(getApplication(), materiaCursando);
                }

                actualizarMaterias();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);

        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.materias_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ArrayList<Carrera> carreras;
        switch(item.getItemId()) {
            case R.id.action_agregar_carrera:
                carreras = new ArrayList<>();

                for (int i= 0; i < sistema.getCarreras().size(); i++) {
                    Carrera carrera = sistema.getCarreras().valueAt(i);

                    if (sistema.getAlumno().getCarreras().get(carrera.getId()) == null) {
                        carreras.add(carrera);
                    }
                }

                if (carreras.size() > 0) {
                    Intent agregar_carrera = new Intent(this, AgregarCarreraActivity.class);

                    agregar_carrera.putExtra("CARRERAS", carreras);

                    startActivityForResult(agregar_carrera, AGREGAR_CARRERA_REQUEST);
                } else {
                    Toast.makeText(this, "No existen carreras para agregar", Toast.LENGTH_SHORT).show();
                }

                return true;
            case R.id.action_borrar_carrera:
                carreras = new ArrayList<>();

                for (int i = 0; i < this.sistema.getAlumno().getCarreras().size(); i++) {
                    Carrera carrera = this.sistema.getAlumno().getCarreras().valueAt(i);

                    carreras.add(carrera);
                }

                if (carreras.size() > 0) {
                    Intent borrar_carrera = new Intent(this, BorrarCarreraActivity.class);

                    borrar_carrera.putExtra("CARRERASALUMNO", carreras);

                    startActivityForResult(borrar_carrera, BORRAR_CARRERA_REQUEST);
                } else {
                    Toast.makeText(this, "No existen carreras para borrar", Toast.LENGTH_SHORT).show();
                }

                return true;
            case R.id.action_agregar_materia_cursar:
                Intent agregar_materia = new Intent(this, AgregarMateriasActivity.class);

                int carreraid = posicion2Carreras.get(comboBoxCarreras.getSelectedItemPosition()).getId();

                agregar_materia.putExtra("CARRERA", sistema.getAlumno().getCarrera(carreraid));

                startActivityForResult(agregar_materia, AGREGAR_MATERIA_REQUEST);

                return true;
            case R.id.action_cerrar_sesion:
                sistema.cerrarSesion(getApplication());

                Intent mainActivity = new Intent(this, MainActivity.class);

                startActivity(mainActivity);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class OyenteCarrera implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String nombre = adapterCarreras.getItem(position);

            if (nombre != null && !nombre.equals("No tiene ninguna carrera")) {
                Carrera carreraSelecccionada = posicion2Carreras.get(position);

                cambioCarrera(carreraSelecccionada);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class OyenteMostrar implements OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            limpiarMaterias();
            Carrera carreraSelecionada = posicion2Carreras.get(position - 1);

            if (sistema.getAlumno().getCarreras().size() > 0) {
                if (parent.getAdapter().getItem(position).equals("Cursando")) {
                    mostrandoMateriasCursando = true;
                    adaptadorMaterias.setMaterias(materiasArrayList(carreraSelecionada, MATERIAS_CURSANDO));
                } else {
                    mostrandoMateriasCursando = false;
                    adaptadorMaterias.setMaterias(materiasArrayList(carreraSelecionada, MATERIAS_TODAS));
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class OyenteMateria implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int posicion = recyclerView.getChildLayoutPosition(v);
            Materia materia = adaptadorMaterias.getMateria(posicion);

            if (mostrandoMateriasCursando) {
                Intent intent = new Intent(MostrarMateriasActivity.this, MostrarMateriaActivity.class);

                intent.putExtra("ALUMNO", sistema.getAlumno());
                intent.putExtra("MATERIA", materia);

                startActivityForResult(intent, MOSTRAR_MATERIA_REQUEST);
            }
        }
    }
}
