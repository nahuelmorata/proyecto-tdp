package morata.nahuel.organizadoruni.materiasActivity.configurarCondicionesActivity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Condicion.Condicion;

public class AdaptadorCondiciones extends RecyclerView.Adapter<AdaptadorCondiciones.CondicionesViewHolder> implements OnClickListener {
    private ArrayList<Class<? extends Condicion>> condiciones;
    private OnClickListener listener;

    public AdaptadorCondiciones(ArrayList<Class<? extends Condicion>> condiciones) {
        this.condiciones = condiciones;
    }

    @NonNull
    @Override
    public CondicionesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_condiciones, viewGroup, false);

        CondicionesViewHolder holder = new CondicionesViewHolder(itemView);

        itemView.setOnClickListener(this);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CondicionesViewHolder condicionesViewHolder, int i) {
        Class<? extends Condicion> condicion = condiciones.get(i);

        condicionesViewHolder.bindCondicion(condicion);
    }

    @Override
    public int getItemCount() {
        return condiciones.size();
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public void setCondiciones(ArrayList<Class<? extends Condicion>> condiciones) {
        this.condiciones = condiciones;
    }

    public class CondicionesViewHolder extends RecyclerView.ViewHolder {
        private TextView lblNombreCondicion;
        private CheckBox checkBoxCondicion;
        private LinearLayout linearLayoutDatos;
        private Class<? extends Condicion> condicion;

        private View viewDatos;

        public CondicionesViewHolder(@NonNull final View itemView) {
            super(itemView);

            this.lblNombreCondicion = itemView.findViewById(R.id.lblNombreCondicion);
            this.checkBoxCondicion = itemView.findViewById(R.id.checkBoxCondicion);
            this.linearLayoutDatos = itemView.findViewById(R.id.layoutCondicionDatos);
        }

        public void bindCondicion(final Class<? extends Condicion> condicion) {
            this.condicion = condicion;
            try {
                lblNombreCondicion.setText((String) condicion.getField("NOMBRE").get(null));

                this.checkBoxCondicion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        try {
                            if (isChecked) {
                                if (viewDatos == null) {
                                    ViewGroup containerDatosPedidos = itemView.findViewById(R.id.layoutCondicionDatos);

                                    int layout = condicion.getField("LAYOUT").getInt(null);

                                    View view = LayoutInflater.from(itemView.getContext()).inflate(layout, containerDatosPedidos, false);

                                    linearLayoutDatos.addView(view);

                                    viewDatos = view;
                                }
                            } else {
                                linearLayoutDatos.removeAllViews();

                                viewDatos = null;
                            }
                        } catch (IllegalAccessException e1) {
                            e1.printStackTrace();
                        } catch (NoSuchFieldException e1) {
                            e1.printStackTrace();
                        }
                    }
                });
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        public Condicion getCondicion() {
            try {
                return condicion.getConstructor(View.class).newInstance(viewDatos);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
