package morata.nahuel.organizadoruni.actualizador;

import android.app.Application;
import android.support.v4.app.FragmentManager;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import org.jetbrains.annotations.NotNull;

import actualizador.ActualizadorQuery;
import morata.nahuel.organizadoruni.Constantes;

public class Actualizador {
    private Application application;

    public Actualizador(Application application) {
        this.application = application;
    }

    public void actualizar(final FragmentManager fragmentManager) {
        ApolloClient.builder().serverUrl(Constantes.URLACTUALIZADOR).build().query(
                ActualizadorQuery.builder().aplicacion("organizadorUni").version(Constantes.VERSION).build()
        ).enqueue(new ApolloCall.Callback<ActualizadorQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<ActualizadorQuery.Data> response) {
                ActualizadorQuery.Actualizacion actualizacion = response.data().actualizacion();
                String url = actualizacion.url();
                String hash = actualizacion.hash();

                if (url != null && hash != null) {
                    ActualizadorDialogFragment actualizadorDialogFragment = new ActualizadorDialogFragment();
                    actualizadorDialogFragment.setUrl(url);
                    actualizadorDialogFragment.setApplication(application);
                    actualizadorDialogFragment.show(fragmentManager, "DialogActualizacion");
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }
}
