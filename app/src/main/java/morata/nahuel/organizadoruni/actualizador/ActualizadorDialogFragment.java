package morata.nahuel.organizadoruni.actualizador;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class ActualizadorDialogFragment extends DialogFragment {
    private String url;
    private Application application;

    public void setUrl(String url) {
        this.url = url;
    }
    public void setApplication(Application application) {
        this.application = application;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Quiere actualizar la aplicacion?.").setTitle("Actualizacion")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new ActualizarAsyncTask(application).execute(url);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }


    private static class ActualizarAsyncTask extends AsyncTask<String, Void, Void> {
        private Application application;

        public ActualizarAsyncTask(Application application) {
            this.application = application;
        }

        @Override
        protected Void doInBackground(String... strings) {
            String url = strings[0];

            FileRequest fileRequest = new FileRequest(url, new Response.Listener<byte[]>() {
                @Override
                public void onResponse(byte[] response) {
                    if (response != null) {
                        File fileUpdate = guardarArchivo(response);

                        Intent intentInstall = new Intent(Intent.ACTION_VIEW)
                                .setDataAndType(Uri.parse("file://" + fileUpdate.getAbsolutePath()), "application/vnd.android.package-archive");

                        intentInstall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        application.startActivity(intentInstall);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(application, new HurlStack());
            requestQueue.add(fileRequest);

            return null;
        }

        private File guardarArchivo(byte[] response) {
            File carpetaOrganizador = new File(Environment.getExternalStorageDirectory() + "/OrganizadorUni/");

            if (!carpetaOrganizador.exists()) {
                carpetaOrganizador.mkdirs();
            }

            File archivoSalida = new File(carpetaOrganizador, "update.apk");
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(archivoSalida);

                fileOutputStream.write(response);

                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return archivoSalida;
        }
    }
}
