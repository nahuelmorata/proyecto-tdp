package morata.nahuel.organizadoruni;

import morata.nahuel.organizadoruni.Sistema.Condicion.CondicionParcialMinimaNota;
import morata.nahuel.organizadoruni.Sistema.Condicion.CondicionSumatoria;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.CursadoParcialesRecuperatorioGeneral;
import morata.nahuel.organizadoruni.Sistema.SistemaPromocion.PromocionDirecta;

public class Constantes {
    public final static int AGREGAR_CARRERA_REQUEST = 1;
    public final static int BORRAR_CARRERA_REQUEST = 2;
    public final static int AGREGAR_MATERIA_REQUEST = 3;
    public final static int CONFIGURAR_MATERIA_REQUEST = 4;
    public final static int MOSTRAR_MATERIA_REQUEST = 5;

    public final static int MATERIAS_CURSANDO = 1;
    public final static int MATERIAS_TODAS = 2;

    public final static String URL = "https://joii.com.ar/organizadorUniAPI/graphql";
    public final static String URLACTUALIZADOR = "https://joii.com.ar/actualizaciones/graphql";

    public final static String VERSION = "1.0.3";

    public final static String[] SISTEMAS_CURSADO = {
            "Parciales + Recuperatorio general",
    };

    public final static String[] SISTEMAS_PROMOCION = {
            "No tiene promoción",
            "Promocion directa",
    };

    public final static Object[][] SISTEMAS_CURSADO_NUEVO = {
            { 0, CursadoParcialesRecuperatorioGeneral.class}
    };

    public final static Object[][] SISTEMAS_PROMOCION_NUEVO = {
            { 0, PromocionDirecta.class}
    };

    public final static Object[][] CONDICIONES_CURSADO = {
            { CondicionParcialMinimaNota.CODIGO, CondicionParcialMinimaNota.class },
            { CondicionSumatoria.CODIGO, CondicionSumatoria.class }
    };

    public final static Object[][] CONDICIONES_RECUPERATORIO = {
            { CondicionParcialMinimaNota.CODIGO, CondicionParcialMinimaNota.class }
    };

    public final static Object[][] CONDICIONES_PROMOCION = {

    };
}
