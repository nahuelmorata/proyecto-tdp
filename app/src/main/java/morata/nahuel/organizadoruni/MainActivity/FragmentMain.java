package morata.nahuel.organizadoruni.MainActivity;

import android.support.v4.app.Fragment;

public interface FragmentMain {
    String getTitulo();
    Fragment getFragmentOpuesto();
}
