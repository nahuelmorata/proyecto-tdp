package morata.nahuel.organizadoruni.MainActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Repositorio.Repositorio;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST = 1;
    private TextView lblAccionMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.controlPermisos();

        this.lblAccionMain = findViewById(R.id.lblAccionMain);
        this.lblAccionMain.setOnClickListener(new OyenteAccion());

        // Cargar del fragment
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentMain, new FragmentLogin());
        fragmentTransaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    new Repositorio(getApplication()).existeUsuario(this);
                } else {
                    finish();
                }
            }
        }
    }

    private void controlPermisos() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST);
        } else {
            new Repositorio(getApplication()).existeUsuario(this);
        }
    }

    class OyenteAccion implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            List<Fragment> fragmentList = fragmentManager.getFragments();

            FragmentMain fragmentMain = (FragmentMain) fragmentList.get(0);

            // Borrar fragment existente
            fragmentManager.beginTransaction().remove((Fragment) fragmentMain).commit();
            lblAccionMain.setText(fragmentMain.getTitulo());

            // Agrego el fragmento contrario
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragmentMain, fragmentMain.getFragmentOpuesto());
            fragmentTransaction.commit();
        }
    }
}
