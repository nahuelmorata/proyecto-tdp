package morata.nahuel.organizadoruni.MainActivity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import org.jetbrains.annotations.NotNull;

import morata.nahuel.organizadoruni.Constantes;
import morata.nahuel.organizadoruni.R;
import organizadorUni.RegistroMutation;

public class FragmentRegistro extends Fragment implements FragmentMain {
    private EditText txtUsuario;
    private EditText txtPassword;
    private EditText txtEmail;
    private Button btnRegistrase;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registrarse, container, false);

        txtUsuario = view.findViewById(R.id.txtUsuarioRegistrarse);
        txtPassword = view.findViewById(R.id.txtPasswordRegistrarse);
        txtEmail = view.findViewById(R.id.txtEmailRegistrarse);
        btnRegistrase = view.findViewById(R.id.btnRegistrarse);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnRegistrase.setOnClickListener(new OyenteRegistro());
    }

    public String getTitulo() {
        return getResources().getString(R.string.registrarse);
    }

    public Fragment getFragmentOpuesto() {
        return new FragmentLogin();
    }

    private class OyenteRegistro implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String usuario = txtUsuario.getText().toString();
            String password = txtPassword.getText().toString();
            String email = txtEmail.getText().toString();

            registro(usuario, password, email);
        }

        private void registro(String usuario, String password, String email) {
            ApolloClient.builder().serverUrl(Constantes.URL).build().mutate(
                    RegistroMutation.builder().usuario(usuario).password(password).email(email).build()
            ).enqueue(new ApolloCall.Callback<RegistroMutation.Data>() {
                @Override
                public void onResponse(@NotNull Response<RegistroMutation.Data> response) {
                    final Activity activity = getActivity();

                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

                                fragmentManager.beginTransaction().remove(FragmentRegistro.this).commit();

                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.add(R.id.fragmentMain, new FragmentLogin());
                                fragmentTransaction.commit();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(@NotNull final ApolloException e) {
                    Activity activity = getActivity();

                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(),"El registro fallo, por favor intente mas tarde", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
        }
    }
}
