package morata.nahuel.organizadoruni.MainActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;

import org.jetbrains.annotations.NotNull;

import morata.nahuel.organizadoruni.Constantes;
import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.materiasActivity.MostrarMateriasActivity;
import organizadorUni.LoginMutation;

public class FragmentLogin extends Fragment implements FragmentMain {
    private Button btnEntrar;
    private EditText txtUsuario;
    private EditText txtPassword;
    private TextView labelMensajes;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        btnEntrar = view.findViewById(R.id.btnEntrar);
        txtUsuario = view.findViewById(R.id.txtUsuario);
        txtPassword = view.findViewById(R.id.txtPass);
        labelMensajes = view.findViewById(R.id.labelMensajes);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.conectarBotones();
    }

    private void conectarBotones() {
        this.btnEntrar.setOnClickListener(new OyenteBotonEntrar());
    }

    @Override
    public String getTitulo() {
        return getResources().getString(R.string.login);
    }

    public Fragment getFragmentOpuesto() {
        return new FragmentRegistro();
    }

    private class OyenteBotonEntrar implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String usuario = txtUsuario.getText().toString();
            String password = txtPassword.getText().toString();

            controlUsuario(usuario, password);
        }

        private void controlUsuario(String usuario, String password) {
            ApolloClient.builder().serverUrl(Constantes.URL).build().mutate(
                    LoginMutation.builder().usuario(usuario).password(password).build()
            ).enqueue(new ApolloCall.Callback<LoginMutation.Data>() {
                @Override
                public void onResponse(@NotNull final Response<LoginMutation.Data> response) {
                    Activity activity = getActivity();

                    if (activity != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LoginMutation.Data dataLogin = response.data();

                                if (dataLogin != null) {
                                    if (dataLogin.login() != null) {
                                        LoginMutation.Login datosLogin = dataLogin.login();

                                        if (datosLogin != null) {
                                            String error = datosLogin.error();
                                            String token = datosLogin.token();

                                            if (error == null && token != null) {
                                                //noinspection ConstantConditions
                                                int id = datosLogin.id();

                                                Intent materias = new Intent(getActivity().getApplicationContext(), MostrarMateriasActivity.class);

                                                materias.putExtra("ID", id);
                                                materias.putExtra("TOKEN", token);
                                                materias.putExtra("NUEVO", true);

                                                getActivity().startActivity(materias);
                                                return;
                                            }
                                        }
                                    }
                                }

                                labelMensajes.setText(R.string.error_ingreso);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(@NotNull ApolloException e) {
                    Activity activity = getActivity();
                    if (activity != null){
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                labelMensajes.setText(R.string.error_ingreso);
                            }
                        });
                    }
                }
            });
        }
    }
}
