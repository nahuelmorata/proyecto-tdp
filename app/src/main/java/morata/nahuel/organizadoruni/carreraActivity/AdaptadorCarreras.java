package morata.nahuel.organizadoruni.carreraActivity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Carrera.Carrera;

public class AdaptadorCarreras extends RecyclerView.Adapter<AdaptadorCarreras.CarreraViewHolder> implements OnClickListener {
    private ArrayList<Carrera> carreras;
    private OnClickListener listener;

    public AdaptadorCarreras(ArrayList<Carrera> carreras) {
        this.carreras = carreras;
    }

    @NonNull
    @Override
    public CarreraViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_carrera, viewGroup, false);

        itemView.setOnClickListener(this);

        return new CarreraViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CarreraViewHolder carreraViewHolder, int pos) {
        Carrera carrera = carreras.get(pos);

        carreraViewHolder.bindCarrera(carrera);
    }

    @Override
    public int getItemCount() {
        return carreras.size();
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public Carrera getCarrera(int posicion) {
        return carreras.get(posicion);
    }

    public void setCarreras(ArrayList<Carrera> carreras) {
        this.carreras = carreras;
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    public class CarreraViewHolder extends RecyclerView.ViewHolder {
        private TextView labelNombre;

        public CarreraViewHolder(@NonNull View itemView) {
            super(itemView);

            labelNombre = itemView.findViewById(R.id.labelNombreCarrera);
        }

        public void bindCarrera(Carrera carrera) {
            labelNombre.setText(carrera.getNombre());
        }
    }
}
