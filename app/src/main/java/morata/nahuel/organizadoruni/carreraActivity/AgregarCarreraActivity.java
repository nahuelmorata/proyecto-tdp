package morata.nahuel.organizadoruni.carreraActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.ArrayList;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Carrera.Carrera;
import morata.nahuel.organizadoruni.carreraActivity.AdaptadorCarreras;

public class AgregarCarreraActivity extends AppCompatActivity {
    protected ArrayList<Carrera> carreras;

    private RecyclerView recyclerViewCarreras;
    private AdaptadorCarreras adaptadorCarreras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_carrera);

        Intent intent = getIntent();

        this.carreras = intent.getParcelableArrayListExtra("CARRERAS");

        this.recyclerViewCarreras = findViewById(R.id.recyclerCarrera);

        this.cargarCarreras();
    }

    private void cargarCarreras() {
        this.adaptadorCarreras = new AdaptadorCarreras(carreras);

        this.adaptadorCarreras.setOnClickListener(new OyenteCarrera());

        recyclerViewCarreras.setHasFixedSize(true);
        recyclerViewCarreras.setAdapter(this.adaptadorCarreras);
        recyclerViewCarreras.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    private class OyenteCarrera implements OnClickListener {

        @Override
        public void onClick(View v) {
            int posicion = recyclerViewCarreras.getChildLayoutPosition(v);

            Carrera carrera = adaptadorCarreras.getCarrera(posicion);

            Intent intent = new Intent();
            intent.putExtra("CARRERA", carrera);

            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }
}
