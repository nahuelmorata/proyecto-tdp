package morata.nahuel.organizadoruni.fragmentos.cursadas.parcialesRecuperatorioGeneral;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.Parcial;

public class AdaptadorMateria extends RecyclerView.Adapter<AdaptadorMateria.MateriaViewHolder> {
    private MateriaCursando materiaCursando;
    private ArrayList<Parcial> parciales;
    private Application application;
    private OnClickListener listener;

    public AdaptadorMateria(MateriaCursando materiaCursando, Application application) {
        this.materiaCursando = materiaCursando;
        this.application = application;

        this.parciales = new ArrayList<>();
        this.parciales.addAll(Arrays.asList(materiaCursando.getSistemaCursado().getParciales()));
    }

    @NonNull
    @Override
    public MateriaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_parcial, viewGroup, false);

        return new MateriaViewHolder(itemView);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull MateriaViewHolder materiaViewHolder, int pos) {
        materiaViewHolder.bindParcial(parciales.get(pos));
    }

    @Override
    public int getItemCount() {
        return parciales.size();
    }

    public class MateriaViewHolder extends RecyclerView.ViewHolder {
        private EditText txtFecha;
        private TextView lblParcial;
        private EditText txtNotaParcial;
        private Button btnGuardarNota;

        public MateriaViewHolder(@NonNull View itemView) {
            super(itemView);

            this.txtFecha = itemView.findViewById(R.id.txtFecha);
            this.lblParcial = itemView.findViewById(R.id.lblParcial);
            this.txtNotaParcial = itemView.findViewById(R.id.txtNotaParcial);
            this.btnGuardarNota = itemView.findViewById(R.id.btnGuardarNota);
        }

        public void bindParcial(Parcial parcial) {
            String textoParcial = itemView.getContext().getString(R.string.texto_parcial, parcial.getNumeracion() + 1);

            this.txtFecha.setText(parcial.getFechaGuardar());
            this.lblParcial.setText(textoParcial);
            this.txtNotaParcial.setText(String.valueOf(parcial.getNota()));
            this.btnGuardarNota.setOnClickListener(new OyenteGuardarNota(parcial));
        }

        private class OyenteGuardarNota implements OnClickListener {
            private Parcial parcial;

            public OyenteGuardarNota(Parcial parcial) {
                this.parcial = parcial;
            }

            @Override
            public void onClick(View v) {
                String fecha = txtFecha.getText().toString();
                int nota = Integer.parseInt(txtNotaParcial.getText().toString());

                this.parcial.setNota(nota);
                this.parcial.setFecha(fecha);

                materiaCursando.guardar(application);

                Toast.makeText(v.getContext(), "Datos del parcial " + (parcial.getNumeracion() + 1) + " guardados", Toast.LENGTH_SHORT).show();

                listener.onClick(v);
            }
        }
    }
}

