package morata.nahuel.organizadoruni.fragmentos.cursadas.parcialesRecuperatorioGeneral;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;
import morata.nahuel.organizadoruni.Sistema.SistemaCursado.CursadoParcialesRecuperatorioGeneral;

public class FragmentoCursadoParcialesRecuperatorioGeneral extends Fragment {
    private RecyclerView recyclerViewParciales;
    private TextView lblTituloMateria;
    private TextView lblMensajeCursado;
    private EditText txtFechaRecuperatorio;
    private TextView lblRecuperatorio;
    private EditText txtNotaRecuperatorio;
    private Button btnGuardarRecuperatorio;
    private TextView lblMensajeRecuperatorio;

    private AdaptadorMateria adaptadorMateria;

    protected MateriaCursando materiaCursando;

    public FragmentoCursadoParcialesRecuperatorioGeneral() { }

    public static FragmentoCursadoParcialesRecuperatorioGeneral newInstance(MateriaCursando materiaCursando) {
        FragmentoCursadoParcialesRecuperatorioGeneral fragment = new FragmentoCursadoParcialesRecuperatorioGeneral();
        Bundle args = new Bundle();

        args.putParcelable("MATERIACURSANDO", materiaCursando);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.materiaCursando = getArguments().getParcelable("MATERIACURSANDO");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cursado_parciales_recuperatorio_general, container, false);

        this.recyclerViewParciales = view.findViewById(R.id.recyclerViewParciales);
        this.lblMensajeCursado = view.findViewById(R.id.lblMensajeRecuperatorioGeneralCursado);

        this.txtFechaRecuperatorio = view.findViewById(R.id.txtFechaRecuperatorio);
        this.txtNotaRecuperatorio = view.findViewById(R.id.txtNotaRecuperatorio);
        this.lblRecuperatorio = view.findViewById(R.id.lblParcialRecuperatorio);
        this.btnGuardarRecuperatorio = view.findViewById(R.id.btnGuardarRecuperatorio);
        this.lblMensajeRecuperatorio = view.findViewById(R.id.lblMensajeRecuperatorio);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.cargarElementosView();

        this.lblTituloMateria.setText(this.materiaCursando.getNombre());

        this.cargarMateriasCursado();
        this.cargarMensaje();

        this.desactivarRecuperatorio();

        if (materiaCursando.getSistemaCursado().terminoParciales()) {
            this.activarRecuperatorio();
        }
    }

    public void cargarElementosView() {
        if (getActivity() != null) {
            this.lblTituloMateria = getActivity().findViewById(R.id.lblTituloMostrarMateria);
        }
    }

    private void cargarMateriasCursado() {
        if (getActivity() != null) {
            this.adaptadorMateria = new AdaptadorMateria(this.materiaCursando, getActivity().getApplication());
        }

        this.recyclerViewParciales.setHasFixedSize(true);
        this.recyclerViewParciales.setAdapter(this.adaptadorMateria);
        this.recyclerViewParciales.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        this.adaptadorMateria.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarMensaje();
            }
        });
    }

    private void cargarMensaje() {
        this.lblMensajeCursado.setText(materiaCursando.getSistemaCursado().control());
    }

    private void cargarRecuperatorio() {
        CursadoParcialesRecuperatorioGeneral sistemaCursado = (CursadoParcialesRecuperatorioGeneral) materiaCursando.getSistemaCursado();

        this.lblRecuperatorio.setText(getResources().getString(R.string.texto_recuperatorio));
        this.txtFechaRecuperatorio.setText(sistemaCursado.getRecuperatorio().getFechaGuardar());
        this.txtNotaRecuperatorio.setText(sistemaCursado.getRecuperatorio().getNota());
        this.btnGuardarRecuperatorio.setOnClickListener(new OyenteGuardarRecuperatorio());
    }

    private void desactivarRecuperatorio() {
        this.lblRecuperatorio.setText(getResources().getString(R.string.texto_recuperatorio));
        this.txtFechaRecuperatorio.setEnabled(false);
        this.txtNotaRecuperatorio.setEnabled(false);
        this.btnGuardarRecuperatorio.setEnabled(false);
    }

    private void activarRecuperatorio() {
        this.txtFechaRecuperatorio.setEnabled(true);
        this.txtNotaRecuperatorio.setEnabled(true);
        this.btnGuardarRecuperatorio.setEnabled(true);

        this.cargarRecuperatorio();
    }

    private class OyenteGuardarRecuperatorio implements OnClickListener {

        @Override
        public void onClick(View v) {
            if (getActivity() != null) {
                materiaCursando.guardar(getActivity().getApplication());
                Toast.makeText(getActivity(), "Recuperatorio guardado", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
