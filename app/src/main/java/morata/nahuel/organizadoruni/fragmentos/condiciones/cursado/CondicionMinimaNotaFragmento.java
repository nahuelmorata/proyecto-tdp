package morata.nahuel.organizadoruni.fragmentos.condiciones.cursado;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import morata.nahuel.organizadoruni.R;

public class CondicionMinimaNotaFragmento extends Fragment {
    public CondicionMinimaNotaFragmento() {}

    public static CondicionMinimaNotaFragmento newInstance() {
        CondicionMinimaNotaFragmento fragment = new CondicionMinimaNotaFragmento();

        Bundle args = new Bundle();

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_condicion_minima_nota, container, false);
    }
}
