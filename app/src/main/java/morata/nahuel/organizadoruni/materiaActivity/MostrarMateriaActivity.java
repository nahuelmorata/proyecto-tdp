package morata.nahuel.organizadoruni.materiaActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import morata.nahuel.organizadoruni.R;
import morata.nahuel.organizadoruni.Sistema.Alumno;
import morata.nahuel.organizadoruni.Sistema.Materia.MateriaCursando;

public class MostrarMateriaActivity extends AppCompatActivity {
    protected MateriaCursando materiaCursando;
    protected Alumno alumno;
    protected Button btnBorraMateriaCursando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_materia);

        Intent intent = getIntent();

        this.materiaCursando = intent.getParcelableExtra("MATERIA");
        this.alumno = intent.getParcelableExtra("ALUMNO");

        this.cargarFragmentoCursada();

        if (this.materiaCursando.esPromocionable()) {
            this.cargarFragmentoPromocion();
        }

        this.cargarBotonBorrar();
    }

    private void cargarFragmentoCursada() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragmento = materiaCursando.getSistemaCursado().getFragmento(this.materiaCursando);

        Bundle bundle = new Bundle();

        bundle.putParcelable("MATERIACURSANDO", this.materiaCursando);

        fragmento.setArguments(bundle);

        fragmentTransaction.add(R.id.scrollViewMateriaCursando, fragmento);
        fragmentTransaction.commit();
    }

    private void cargarFragmentoPromocion() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragmento = materiaCursando.getSistemaPromocion().getFragmento(this.materiaCursando);

        Bundle bundle = new Bundle();

        bundle.putParcelable("MATERIACURSANDO", this.materiaCursando);

        fragmento.setArguments(bundle);

        fragmentTransaction.add(R.id.scrollViewMateriaCursando, fragmento);
        fragmentTransaction.commit();
    }

    private void cargarBotonBorrar() {
        this.btnBorraMateriaCursando = findViewById(R.id.btnBorrarMateriaCursando);
        this.btnBorraMateriaCursando.setOnClickListener(new OyenteBorrarMateriaCursando());
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);

        finish();
    }

    private class OyenteBorrarMateriaCursando implements OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent();

            intent.putExtra("MATERIACURSADOBORRAR", materiaCursando);

            setResult(RESULT_OK, intent);

            finish();
        }
    }
}
